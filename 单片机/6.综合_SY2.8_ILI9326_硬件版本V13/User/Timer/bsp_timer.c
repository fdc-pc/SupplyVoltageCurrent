
#include "bsp_timer.h"

/*********************************************************************************
* 函数名称: bsp_InitTimer, bsp_ResetTimer
* 功    能: 初始化所有的定时器
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitTimer(void)
{
    TIM2_Config();
    TIM3_Config();
    TIM4_Config();
    NVIC_Config();
}
void bsp_ResetTimer(void)
{
    TIM_Cmd(TIM2, ENABLE);
    TIM_Cmd(TIM3, ENABLE);
    TIM_Cmd(TIM4, ENABLE);
}
/*********************************************************************************
* 函数名称: NVIC_Config
* 功    能: NVIC配置
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void NVIC_Config(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    /* Configure one bit for preemption priority */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);
    //USART_DMA
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
    //USART_DMA RX
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
    //USART_DMA TX
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
    //USART帧检测，回调
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
    //TIM2按键检测以及DelayMs()函数
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
    //TIM3 ADC数据更新显示
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( & NVIC_InitStructure);
}


/*********************************************************************************
* 函数名称: TIM2_Config
* 功    能: 定时器2初始化,1ms
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void TIM2_Config(void)
{
    //设置定时器周期 72MHz/3600/20 = 1KHz
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseStructure.TIM_Period    = 19;
    TIM_TimeBaseStructure.TIM_Prescaler = 3599;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM2, & TIM_TimeBaseStructure);
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM2, DISABLE);
}

/*********************************************************************************
* 函数名称: TIM3_Config
* 功    能: 定时器3初始化10ms
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void TIM3_Config(void)
{
    //设置定时器周期 72MHz/3600/200 = 100Hz
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    TIM_TimeBaseStructure.TIM_Period    = 199;
    TIM_TimeBaseStructure.TIM_Prescaler = 3599;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM3, & TIM_TimeBaseStructure);
    TIM_ClearFlag(TIM3, TIM_FLAG_Update);
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM3, DISABLE);
}

/*********************************************************************************
* 函数名称: TIM4_Config
* 功    能: 定时器3初始化5ms
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void TIM4_Config(void)
{
    //设置定时器周期 72MHz/3600/100 = 200Hz
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    TIM_TimeBaseStructure.TIM_Period    = 99;
    TIM_TimeBaseStructure.TIM_Prescaler = 3599;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM4, & TIM_TimeBaseStructure);
    TIM_ClearFlag(TIM4, TIM_FLAG_Update);
    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM4, DISABLE);
}

