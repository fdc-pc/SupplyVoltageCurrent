
#ifndef __BSP_TIMER_H
#define __BSP_TIMER_H
#include "stm32f10x.h"

//内部调用函数
static void TIM2_Config(void);  //DelayMs, Button, DisplayUpdate
static void TIM3_Config(void);  //NC
static void TIM4_Config(void);  //Usart_DMA
static void NVIC_Config(void);

//外部调用函数
void bsp_InitTimer(void);
void bsp_ResetTimer(void);
#endif
