
#include "stm32f10x.h"
#include "bsp_board.h"

extern void RxCallBack(void * pParam, const u8 * buf, u16 len);
/*********************************************************************************
* 函数名称: bsp_InitAllGpio
* 功    能: 初始化IO端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitAllDevice(void)
{
    RCC_Config();       //初始化所有的时钟频率
    bsp_InitTimer();    //初始化所有的定时器
    bsp_InitADC();      //电压、电流、温度采集。注意得先关闭定时器否则会进入HardFault，为什么？
    bsp_InitButton();   //初始化按钮：74HC595
    bsp_InitDAC();      //电压、电流DA初始化
    bsp_InitFan();      //初始化风机端口
    bsp_InitI2C();      //初始化I2C, EEPROM保存数据
    bsp_InitOutput();   //输出控制端口初始化
    bsp_InitRelay();    //初始化继电器
    bsp_InitUsart(RxCallBack, 0);
    bsp_InitUsartPrintf();//配置USART3为发送模式，因为如果配置接受引脚input floating的话，必须连上串口才能工作？？？？？！！！
}
void bsp_ResetAllDevice(void)
{
    bsp_ResetTimer();   //开启所有的定时器
    DelayMs(100);
    bsp_InitLcd();
    bsp_ResetADC();     //数组清零
    bsp_ResetButton();  //74HC595 SER等复位
    bsp_ResetDAC();     //DA输出清零
    bsp_ResetFan();     //关风机
    bsp_ResetI2C();     //写禁止
    bsp_ResetOutput();  //关输出
    bsp_ResetRelay();   //关继电器
    bsp_ResetUsart();   //
    bsp_ResetLcd();     //白色清屏
}

/*********************************************************************************
* 函数名称: RCC_Config
* 功    能: 设置各个系统时钟
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void RCC_Config(void)
{
    RCC_PCLK1Config(RCC_HCLK_Div4);  // PCLK1 = HCLK/4 //
    RCC_ADCCLKConfig(RCC_PCLK2_Div8);// ADCCLK = PCLK2/8//
    //下面是给各外设开启时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);//提供ms延时，按键检测
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);//定时刷新ADC数据
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    //打开所有GPIO的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC
                           | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);
    //启动DAC时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
    //启动串口1时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    //启动AFIO时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    //启动ADC1和ADC2,ADC3时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_ADC2 | RCC_APB2Periph_ADC3, ENABLE);
    //启动DMA1时钟：用于ADC1, ADC2 DMA传输
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
}

/*********************************************************************************
* 函数名称: SysReset
* 功    能: 主机复位
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void SysReset(void)
{
    DelayMs(1000);
    //WDR();
    /* WWDG configuration */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);/* Enable WWDG clock */
    WWDG_SetPrescaler(WWDG_Prescaler_8);/* WWDG clock counter = (PCLK1/4096)/8 = 244 Hz (~4 ms)  */
    WWDG_SetWindowValue(100);   /* Set Window value to 100 */
    WWDG_Enable(127);   /* Enable WWDG and set counter value to 127, WWDG timeout = ~4 ms * 64 = 262 ms */
    WWDG_ClearFlag();    /* Clear EWI flag */
    while (1);
}
