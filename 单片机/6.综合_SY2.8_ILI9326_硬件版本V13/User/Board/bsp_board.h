/* 文件包含 -------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "GpioConfig.h"

#include "bsp_adc.h"
#include "bsp_button.h"
#include "bsp_dac.h"
#include "bsp_fan.h"
#include "bsp_i2c.h"
#include "bsp_output.h"
#include "bsp_relay.h"
#include "bsp_timer.h"
#include "bsp_usart.h"
#include "bsp_lcd_gui.h"
#include "bsp_usartprintf.h"

#include "calibrate.h"  //校准
#include "onbutton.h"   //按键响应
#include "oncmd.h"      //串口通讯协议解析
#include <stdio.h>
//内部调用
static void RCC_Config(void);

//外部调用
void bsp_InitAllDevice(void);
void bsp_ResetAllDevice(void);
void SysReset(void);
