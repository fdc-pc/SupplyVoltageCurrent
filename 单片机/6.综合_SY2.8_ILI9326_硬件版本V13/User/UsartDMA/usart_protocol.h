
#ifndef __USART_PROTOCOL_H
#define __USART_PROTOCOL_H

//------------通讯格式定义-------------//
#define USART_FRAMEHEAD     0xAA
#define USART_FRAMETAIL     0x55
#define USART_FRAMECTRL     0xA5

#define USART_FRAME_SIZE    5   //与上位机通讯一帧的字节数
#define MAX_FRAME_SIZE    100   //与上位机通讯一帧的最大字节数（含帧头、帧尾、控制符、校验等）

typedef enum {
    //M2S: master to slave cmd, S2M: slave to master
    USART_M2S_Output  = 0x01,  //输出控制
    USART_M2S_Mode    = 0x02,  //输出模式切换（默认电压模式）
    USART_M2S_SetVal  = 0x03,  //Master2Slave设置期望值:[1-4]:电压; [5-8]:电流
    USART_M2S_Cal     = 0x04,  //进入校准
    USART_M2S_StepAdd = 0x05,  //步进加
    USART_M2S_StepSub = 0x06,  //步进减
    USART_M2S_FAN     = 0x07,  //风扇控制:[1]0关;1开;2自动(默认)
    USART_M2S_STATE   = 0x08,  //实时数据发送状态:[1]1发送数据 0停止发送
    USART_M2S_GetVal  = 0x09,  //获取前一次的设定值
    USART_S2M_PreVal  = 0x0A,  //Slave2Master返回前一次的设定值:[1-4]:电压; [5-8]:电流
    USART_S2M_RealVal = 0x0B,  //Slave2Master返回实际值:[1-4]:电压; [5-8]:电流
    USART_S2M_RealTemp= 0x0C,  //Slave2Master返回温度值:[1-4]:温度
} USART_CMD_TYPE;

#endif
