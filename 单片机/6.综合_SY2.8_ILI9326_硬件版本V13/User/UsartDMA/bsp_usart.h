
#ifndef __BSP_USART_H
#define __BSP_USART_H
#include "stm32f10x.h"
#include "usart_protocol.h"

//------------USART端口定义----------//
#define USART_TX_PORT      GPIOA
#define USART_TX_CLK       RCC_APB2Periph_GPIOA
#define USART_TX_PIN       GPIO_Pin_9

#define USART_RX_PORT      GPIOA
#define USART_RX_CLK       RCC_APB2Periph_GPIOA
#define USART_RX_PIN       GPIO_Pin_10

//------------ DMA Config -----------//
#define DMA_RX_Channel       DMA1_Channel5
#define DMA_RX_IRQHandler    DMA1_Channel5_IRQHandler
#define DMA_TX_Channel       DMA1_Channel4
#define DMA_TX_IRQHandler    DMA1_Channel4_IRQHandler

#define USART_SEND_BUF_LEN    MAX_FRAME_SIZE
#define USART_RECE_BUF_LEN    MAX_FRAME_SIZE

typedef struct {
    u8 sendBuf[USART_SEND_BUF_LEN];
    u8 receBuf[USART_RECE_BUF_LEN];

    u8  receHead;
    u8  receTail;

    u8  state;
    u8  transmittingFlag;
} USART_PORT;

//typedef struct{
//    u8 frameId; //帧ID
//    u8 *pData;  //数组指针
//    u8 dataLen; //数组长度
//}USART_FRAME;   //帧格式定义

typedef enum {
    MY_FALSE = 0,
    MY_TRUE = 1,
} MY_BOOL;

typedef void ( * UsartRxCallBackFunc)(void * pParam, const u8 * byBuf, u16 dwLen);

//extern volatile u32 m_UsartRxLostFrame; //总的丢包
//extern volatile u8  m_UsartRxFrameLen;  //解析成功的帧的长度
//extern volatile u8  m_UsartRxFrameBuf[MAX_FRAME_SIZE]; //解析后的帧数据存放数组

//extern volatile MY_BOOL isRxNewFrame;
//extern USART_PORT  UsartPort1;

//extern DMA_InitTypeDef     DMA_InitStructureTx;
//extern DMA_InitTypeDef     DMA_InitStructureRx;
//extern vu8 USART1_Rx_Buff[MAX_FRAME_SIZE];
//extern vu8 USART1_Tx_Buff[MAX_FRAME_SIZE];//发送缓冲区

//extern UsartRxCallBackFunc m_UsartRxCallBackFunc;
//extern void * m_pUsartRxCallBackParam;

//内部调用函数
static void USART1_Config(void);
static void DMA_RxConfig(void);
static void DMA_TxConfig(void);
static MY_BOOL AnalyzePackage(u8 data);
static void detectFrame(void);
static void SetUsartRxCallBack(UsartRxCallBackFunc func, void * pParam);//设置回调函数

//外部调用函数
void bsp_InitUsart(UsartRxCallBackFunc func, void * pParam);   //初始化串口：端口、DMA
void bsp_ResetUsart(void);
void bsp_UsartSendFrame(u8 array[], u8 size);   //发送一帧数据

#endif
