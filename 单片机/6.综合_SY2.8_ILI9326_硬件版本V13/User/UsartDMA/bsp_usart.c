
#include "bsp_usart.h"
//接收未知长度的数据，只有一个起点，即串口数据接收中断，在此函数中初始化DMA开始传输。
//终点无非两个，
//一：DMA没有接收完成，串口进入总线空闲中断，在空闲中断中获取收到的数据量，然后结束DMA传输；
//二：DMA接收完成，进入DMA接收完成中断，关闭DMA接收。
//在其中涉及到了各种中断的切换：
//一：触发串口接收中断后，关闭串口接收中断，打开串口总线空闲中断和DMA传输完成中断。
//二：进入DMA完成中断后，关闭总线空闲中断，打开串口接收中断，准备下一次接收，同时结束本次DMA传输，
//三：如果进入了总线空闲中断，打开串口接收中断，准备下一次接收，同时结束本次DMA传输。
//考虑到HEX数据流，缓冲结构体内引入了指示有效数据长度的变量，缓冲结构体引入了乒乓操作，数据被依次注入
//到结构体中的缓冲数组内，同时为了只是通道的可读性，引入了标志位。结构体内的标志位对CPU来说都是"只读"的，修改将导致错误。

DMA_InitTypeDef     DMA_InitStructureTx;
DMA_InitTypeDef     DMA_InitStructureRx;

vu8 USART1_Rx_Buff[MAX_FRAME_SIZE];
vu8 USART1_Tx_Buff[MAX_FRAME_SIZE];
volatile MY_BOOL isRxNewFrame = MY_FALSE;
USART_PORT  UsartPort1;

UsartRxCallBackFunc m_UsartRxCallBackFunc;
void * m_pUsartRxCallBackParam;

/*********************************************************************************
* 函数名称: bsp_InitUsart
* 功    能: 串口1初始化，波特率：115200
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitUsart(UsartRxCallBackFunc func, void * pParam)
{
    USART1_Config();
    DMA_RxConfig();
    DMA_TxConfig();
    SetUsartRxCallBack(func, pParam);
}
void bsp_ResetUsart(void)
{
}

/*********************************************************************************
* 函数名称: bsp_SetUsartRxCallBack
* 功    能: 设置接收帧处理的回调函数
* 参    数: 外部设置的回调函数，参数
* 返 回 值: 无
**********************************************************************************/
void SetUsartRxCallBack(UsartRxCallBackFunc func, void * pParam)
{
    m_pUsartRxCallBackParam = pParam;
    m_UsartRxCallBackFunc   = func;
}

/*********************************************************************************
* 函数名称: bsp_UsartSendFrame
* 功    能: 将帧发送出去
* 参    数: 帧
* 返 回 值: 无
**********************************************************************************/
void bsp_UsartSendFrame(u8 array[], u8 size)
{
    u8 len = 0, iter, m_CheckSum;
    USART1_Tx_Buff[0] = USART_FRAMEHEAD;
    USART1_Tx_Buff[1] = USART_FRAMEHEAD;
    len = 2;
    m_CheckSum = 0;
    for (iter = 0; iter < size; iter++) {
        if (array[iter] == USART_FRAMECTRL || array[iter] == USART_FRAMEHEAD || array[iter] == USART_FRAMETAIL) {
            USART1_Tx_Buff[len] = USART_FRAMECTRL;
            len ++;
        }
        USART1_Tx_Buff[len] = array[iter];
        len ++;
        m_CheckSum += array[iter];
    }
    if (m_CheckSum == USART_FRAMECTRL || m_CheckSum == USART_FRAMEHEAD || m_CheckSum == USART_FRAMETAIL) {
        USART1_Tx_Buff[len] = USART_FRAMECTRL;
        len ++;
    }
    USART1_Tx_Buff[len] = m_CheckSum;
    len++;
    USART1_Tx_Buff[len] = USART_FRAMETAIL;
    len++;
    USART1_Tx_Buff[len] = USART_FRAMETAIL;
    len++;
    DMA_InitStructureTx.DMA_BufferSize = len;
    DMA_Init(DMA_TX_Channel, & DMA_InitStructureTx);
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    DMA_Cmd(DMA_TX_Channel, ENABLE);
}

//================================================================================//

/*********************************************************************************
* 函数名称: USART1_Config
* 功    能: 串口1初始化，波特率：115200
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void USART1_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_APB2PeriphClockCmd(USART_TX_CLK | USART_RX_CLK | RCC_APB2Periph_USART1, ENABLE);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;//配置串口1Rx端口输入高阻
    GPIO_InitStructure.GPIO_Pin = USART_RX_PIN;
    GPIO_Init(USART_RX_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;    //配置串口1Tx端口输出推挽
    GPIO_InitStructure.GPIO_Pin   = USART_TX_PIN;
    GPIO_Init(USART_TX_PORT, & GPIO_InitStructure);
    //配置串口
    USART_InitStructure.USART_BaudRate   = 115200;              //配置波特率9600
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; //配置8bit长度
    USART_InitStructure.USART_StopBits   = USART_StopBits_1;    //配置1位停止位
    USART_InitStructure.USART_Parity     = USART_Parity_No;     //配置无奇偶效验
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //配置无流控制
    USART_InitStructure.USART_Mode       = USART_Mode_Rx | USART_Mode_Tx;           //配置异步串口收发模式
    USART_Init(USART1, & USART_InitStructure);
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ClearFlag(USART1, USART_FLAG_RXNE);
    //USART_SmartCardCmd(USART1,DISABLE);
    /* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
    如下语句解决第1个字节无法正确发送出去的问题 */
    USART_ClearFlag(USART1, USART_FLAG_TC);
    USART_DMACmd(USART1, USART_DMAReq_Tx | USART_DMAReq_Rx, ENABLE);
    USART_Cmd(USART1, ENABLE);                                                    //使能USART1
}

static void DMA_RxConfig(void)
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    DMA_DeInit(DMA_RX_Channel);
    DMA_InitStructureRx.DMA_PeripheralBaseAddr = (u32)( & (USART1->DR));
    DMA_InitStructureRx.DMA_MemoryBaseAddr     = (u32)( & (USART1_Rx_Buff[0]));
    DMA_InitStructureRx.DMA_DIR                = DMA_DIR_PeripheralSRC;
    DMA_InitStructureRx.DMA_BufferSize     = MAX_FRAME_SIZE;
    DMA_InitStructureRx.DMA_PeripheralInc  = DMA_PeripheralInc_Disable;
    DMA_InitStructureRx.DMA_MemoryInc      = DMA_MemoryInc_Enable;
    DMA_InitStructureRx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructureRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructureRx.DMA_Mode           = DMA_Mode_Normal;
    DMA_InitStructureRx.DMA_Priority       = DMA_Priority_High;
    DMA_Init(DMA_RX_Channel, & DMA_InitStructureRx);
}
static void DMA_TxConfig(void)
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);//DMA1
    DMA_DeInit(DMA_TX_Channel);
    DMA_InitStructureTx.DMA_PeripheralBaseAddr = (u32)( & (USART1->DR));        //USART1->DR; 外设地址
    DMA_InitStructureTx.DMA_MemoryBaseAddr = (u32)( & USART1_Tx_Buff);          //内存地址
    DMA_InitStructureTx.DMA_DIR            = DMA_DIR_PeripheralDST;             //传输方向
    DMA_InitStructureTx.DMA_BufferSize     = MAX_FRAME_SIZE;                    //设置DMA传输时缓冲区大小
    DMA_InitStructureTx.DMA_PeripheralInc  = DMA_PeripheralInc_Disable;         //设置DAM外设地址递增模式，因为目前就一个外设，故此处设置为禁止
    DMA_InitStructureTx.DMA_MemoryInc      = DMA_MemoryInc_Enable;              //内存地址递增
    DMA_InitStructureTx.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;   //8位数据位宽度
    DMA_InitStructureTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructureTx.DMA_Mode           = DMA_Mode_Normal;             //这里必须设置为这个模式
    DMA_InitStructureTx.DMA_Priority       = DMA_Priority_High;             //设置优先级
    DMA_Init(DMA_TX_Channel, & DMA_InitStructureTx);
    //能相应中断控制位前，对应标志需要清零；否则一旦使能就立刻产生中断
    DMA_ITConfig(DMA_TX_Channel, DMA_IT_TC, ENABLE);
    DMA_ITConfig(DMA_TX_Channel, DMA_IT_TE, ENABLE);
    DMA_ClearITPendingBit(DMA1_IT_TE4);      //清标志
    DMA_ClearITPendingBit(DMA1_IT_TC4);      //清标志
    //TCIFx ，传输完成      ； 中断使能控制位：TCIE
    //HTIFx ，传输完成一半  ； 中断使能控制位：HTIE
    //TEIFx ，传输错误      ； 中断使能控制位：TEIE
    //FEIFx ，FIFO错误      ； 中断使能控制位：FEIE
    //DMEIFx ，Direct模式错误    ； 中断使能控制位：DMEIE
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    DMA_Cmd(DMA_TX_Channel, DISABLE); //注意:这里不能使能DMA,否则会在第一帧发送的第一个字节出错的
}

/*********************************************************************************
* 函数名称: USART1_IRQHandler
* 功    能: 中断函数
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void USART1_IRQHandler(void)
{
    u16 iter = 0;
    u16 len = 0;
    if (USART_GetITStatus(USART1, USART_IT_RXNE)) {
        //清除中断挂起
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        //屏蔽接收中断，开启IDLE中断
        USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
        USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
        //不修改地址与缓冲区大小
        DMA_Init(DMA_RX_Channel, & DMA_InitStructureRx);
        DMA_ITConfig(DMA_RX_Channel, DMA_IT_TC, ENABLE);
        DMA_Cmd(DMA_RX_Channel, ENABLE);
    } else if (USART_GetITStatus(USART1, USART_IT_IDLE)) {
        //清除中断挂起
        USART_ClearITPendingBit(USART1, USART_IT_IDLE);
        //接收完成，恢复接收中断，准备下一次接收
        USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);
        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
        len = MAX_FRAME_SIZE - DMA_GetCurrDataCounter(DMA_RX_Channel);
        for (iter = 0; iter < len; iter++) {
            UsartPort1.receBuf[UsartPort1.receHead] = USART1_Rx_Buff[iter];
            UsartPort1.receHead = (UsartPort1.receHead + 1) % USART_RECE_BUF_LEN;
        }
        DMA_Cmd(DMA_RX_Channel, DISABLE);
        isRxNewFrame = MY_TRUE;
    }
}

/*********************************************************************************
* 函数名称: DMA_RX_IRQHandler
* 功    能: 接收数组满中断函数
* 参    数: 无
* 返 回 值: 无
* 说    名：由于Rx缓冲区不会存满，所以DMA_RX_IRQHandler不会执行!!!!!!!
**********************************************************************************/
void DMA_RX_IRQHandler(void)
{
    if (DMA_GetITStatus(DMA1_IT_TC5)) {
        //清除中断挂起
        DMA_ClearITPendingBit(DMA1_IT_TC5);
        DMA_DeInit(DMA_RX_Channel);
        //关闭IDLE中断，打开接收中断，为下一次数据准备，如果产生DMA传输中断的同时，
        //又触发了总线空闲中断，即，通道填满后数据流恰好结束，如果不禁止IDLE中断，
        //就会发生两次接收结束，两个函数都处理了数据就会导致错误
        USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);
        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    }
}

/*********************************************************************************
* 函数名称: DMA_TX_IRQHandler
* 功    能: 发送中断
* 参    数: 无
* 返 回 值: 无
* 说    名：无
**********************************************************************************/
void DMA_TX_IRQHandler(void)
{
    if (DMA_GetITStatus(DMA1_IT_TC4) != RESET) {
        DMA_ClearITPendingBit(DMA1_IT_TC4);
        USART_DMACmd(USART1, USART_DMAReq_Tx, DISABLE);
        DMA_Cmd(DMA_TX_Channel, DISABLE);
    }
}

/*********************************************************************************
* 函数名称: AnalyzePackage
* 功    能: 解析接收到的数据
* 参    数: 无
* 返 回 值: 无
* 说    名：无
**********************************************************************************/
volatile u32 m_UsartRxLostFrame = 0;
volatile u8  m_UsartRxFrameLen = 0;
volatile u8  m_UsartRxFrameBuf[MAX_FRAME_SIZE];
static MY_BOOL AnalyzePackage(u8 data)
{
    static u8 lastByte = 0;
    static MY_BOOL beginFlag = MY_FALSE;
    static MY_BOOL ctrlFlag = MY_FALSE;
    static u16 revOffset = 0;
    static u8 CheckSum = 0;
    if (((data == USART_FRAMEHEAD) && (lastByte == USART_FRAMEHEAD)) || (revOffset > MAX_FRAME_SIZE)) {
        if (revOffset < (USART_FRAME_SIZE + 1) && revOffset > 0) {
            m_UsartRxLostFrame ++;
        }
        revOffset = 0;
        beginFlag = MY_TRUE;
        lastByte = data ;
        return MY_FALSE;
    }
    if ((data == USART_FRAMETAIL) && (lastByte == USART_FRAMETAIL) && beginFlag) {
        revOffset--;//得到去除头尾和控制符的数据的个数
        m_UsartRxFrameLen = revOffset - 1;
        CheckSum -= USART_FRAMETAIL;
        CheckSum -= m_UsartRxFrameBuf[m_UsartRxFrameLen];
        lastByte = data;
        beginFlag = MY_FALSE;
        if (CheckSum == m_UsartRxFrameBuf[m_UsartRxFrameLen]) {
            CheckSum = 0;
            return MY_TRUE;
        }
        m_UsartRxLostFrame ++;
        CheckSum = 0;
        return MY_FALSE;
    }
    lastByte = data ;
    if (beginFlag) {
        if (ctrlFlag) {
            m_UsartRxFrameBuf[revOffset++] = data;
            CheckSum += data;
            ctrlFlag = MY_FALSE;
            lastByte = USART_FRAMECTRL;
        } else if (data == USART_FRAMECTRL) {
            ctrlFlag = MY_TRUE;
        } else {
            m_UsartRxFrameBuf[revOffset++] = data;
            CheckSum += data;
        }
    }
    return MY_FALSE;
}

/*********************************************************************************
* 函数名称: detectFrame
* 功    能: 检测是否接收到一帧
* 参    数: 无
* 返 回 值: 无
* 说    名：无
**********************************************************************************/
static void detectFrame(void)
{
    if (isRxNewFrame == MY_TRUE) {
        isRxNewFrame = MY_FALSE;
        while (UsartPort1.receHead != UsartPort1.receTail) {
            if (AnalyzePackage(UsartPort1.receBuf[UsartPort1.receTail]) == MY_TRUE) {
                m_UsartRxCallBackFunc(m_pUsartRxCallBackParam, (u8 *)m_UsartRxFrameBuf, m_UsartRxFrameLen);
            }
            UsartPort1.receTail = (UsartPort1.receTail + 1) % USART_RECE_BUF_LEN;
        }
    }
}

/*********************************************************************************
* 函数名称: TIM4_IRQHandler
* 功    能: TIM4的中断函数，检测是否接收到一帧数据
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void TIM4_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) {
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
        detectFrame();
    }
}
