
#include "calibrate.h"


/*********************************************************************************
* 函数名称: void CalibrateVoltage(float voltage, u16 *adValue, u16 *daValue)
* 功    能: 校准电压函数
           //调节旋钮加减电压电流，使输出电压和校准值一致
* 参    数: voltage:设定电压, adValue:返回AD值，daValue返回DA值
* 返 回 值: 无
**********************************************************************************/
void CalibrateVoltage(float voltage, vu16 * adValue, vu16 * daValue)
{
    u16 daVal = bsp_SetDacU(voltage);
    bsp_SetRelay(voltage);
    bsp_SetDacI(0.50);
    OUT_ON();
    while (1) {
        if (stepAddPressed) { //++
            stepAddPressed = 0;
            daVal++;
            if (daVal <= 1) {
                daVal = 4095;
            }
            DAC_SetChannel1Data(DAC_Align_12b_R, daVal);
        }
        if (stepSubPressed) { //--
            stepSubPressed = 0;
            daVal--;
            if (daVal > 4094) {
                daVal = 0;
            }
            DAC_SetChannel1Data(DAC_Align_12b_R, daVal);
        }
        if (calBtnPressed) { //按CAL键退出校正
            calBtnPressed = 0;
            DelayMs(200);
            * adValue = bsp_ReadCalAD(0);  //读取AD值
            printf("voltage: %f    DA: %d    AD: %d\r\n", voltage, daVal, * adValue);
            break;//退出校准
        }
    }
    * daValue = daVal;
    OUT_OFF();
}

/*********************************************************************************
* 函数名称: void CalibrateCurrent(float current, u16 *adValue, u16 *daValue)
* 功    能: 校准电流函数
           //调节旋钮加减电压电流，使输出电压和校准值一致
* 参    数: current:设定电流, adValue:返回AD值，daValue返回DA值
* 返 回 值: 无
**********************************************************************************/
void CalibrateCurrent(float current, vu16 * adValue, vu16 * daValue)
{
    u16 daVal = bsp_SetDacI(current); //????DA?
    bsp_SetDacU(5.0);
    bsp_SetRelay(5.0);
    OUT_ON();
    while (1) {
        if (stepAddPressed) { //++
            stepAddPressed = 0;
            daVal++;
            if (daVal <= 1) {
                daVal = 4095;
            }
            printf("%d\r\n", daVal);
            DAC_SetChannel2Data(DAC_Align_12b_R, daVal);
        }
        if (stepSubPressed) { //--
            stepSubPressed = 0;
            daVal--;
            if (daVal > 4094) {
                daVal = 0;
            }
            DAC_SetChannel2Data(DAC_Align_12b_R, daVal);
        }
        if (calBtnPressed) { //按CAL键退出校正
            calBtnPressed = 0;
            DelayMs(200);
            * adValue = bsp_ReadCalAD(1);  //读取AD值
            printf("current: %f    DA: %d    AD: %d\r\n", current, daVal, * adValue);
            break;//退出校准
        }
    }
    * daValue = daVal;
    OUT_OFF();
}




/*********************************************************************************
* 函数名称: Calibrate()  !!!!!!!!!!!!!obsolete!!!!!!
* 功    能: 校准函数
           calStage: 校准阶段
           0：电压0.100V
           1：电压MAX_VOLTAGE/100V
           2：电流0.100A
           3：电流MAX_CURRENT/1000A
           //调节旋钮加减电压电流，使输出电压和校准值一致
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void Calibrate(void)
{
    vu8  calStage = 0, iter = 0;
    //vu16 U, I, AD;
    vu16 AD;
    u8  Buff[10];
    u8  DAcalBuff[10];//电压校准数据缓冲
    u8  ADcalBuff[10];//电压校准数据缓冲
    //    //再检测校准键一次
    //    for (iter = 100; iter > 0; iter --) {
    //        DelayMs(20);
    //        if (calBtnPressed)  {
    //            calBtnPressed = 0;
    //            break;
    //        }
    //    }
    //    if (iter == 0) {
    //        goto END_CAL;//退出校准
    //    }
    //校正前的数据
    DAcalBuff[0]  = 0x55;
    DAcalBuff[1]  = 0xAA;
    DAcalBuff[2] = (u8)(DAUcal01 >> 8); //电压0.1V高位
    DAcalBuff[3] = (u8)(DAUcal01);      //电压0.1V低位
    DAcalBuff[4] = (u8)(DAUcal30 >> 8); //最大电压高位
    DAcalBuff[5] = (u8)(DAUcal30);      //最大电压低位
    DAcalBuff[6] = (u8)(DAIcal01 >> 8); //电流0.1A高位
    DAcalBuff[7] = (u8)(DAIcal01);      //电流0.1A高位
    DAcalBuff[8] = (u8)(DAIcal4 >> 8);  //最大电流高位
    DAcalBuff[9] = (u8)(DAIcal4);       //最大电流高位
    ADcalBuff[0]  = 0x55;
    ADcalBuff[1]  = 0xAA;
    ADcalBuff[2] = (u8)(ADUcal01 >> 8); //电压0.1V高位
    ADcalBuff[3] = (u8)(ADUcal01);      //电压0.1V低位
    ADcalBuff[4] = (u8)(ADUcal30 >> 8); //最大电压高位
    ADcalBuff[5] = (u8)(ADUcal30);      //最大电压低位
    ADcalBuff[6] = (u8)(ADIcal01 >> 8); //电流0.1A高位
    ADcalBuff[7] = (u8)(ADIcal01);      //电流0.1A高位
    ADcalBuff[8] = (u8)(ADIcal4 >> 8);  //最大电流高位
    ADcalBuff[9] = (u8)(ADIcal4);       //最大电流高位
    //开始校准：0.10V-->MAX_VOLTAGE/100V-->0.10A-->MAX_CURRENT/1000A
    FAN_ON();
    DisplayCalibrateGUI(calStage);//------显示校准菜单
    bsp_SetDacU(0.100);
    bsp_SetRelay(0.100);
    bsp_SetDacI(1.000);
    while (1) {
        if (calBtnPressed) {
            calBtnPressed = 0;
            if (calStage == 0) {
                DelayMs(200);
                AD = bsp_ReadCalAD(0);//读取校准0.1V时的一次电压值
                ADcalBuff[2] = (u8)(AD >> 8);       //电压0.1V高位
                ADcalBuff[3] = (u8)(AD);            //电压0.1V低位
                DAcalBuff[2] = (u8)(DAUcal01 >> 8); //电压0.1V高位
                DAcalBuff[3] = (u8)(DAUcal01);      //电压0.1V低位
                if (I2C_BufferWrite(DAcalBuff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "DAUcal01 Saved!");
                } else {
                    AddNewLog(LOG_LEFT, "DAUcal01 Failed");
                }
                DelayMs(100);
                if (I2C_BufferWrite(ADcalBuff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal AD Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
            }
            if (calStage == 1) {
                DelayMs(200);
                AD = bsp_ReadCalAD(0);//读取校准30V时的一次电压值
                ADcalBuff[4] = (u8)(AD >> 8);       //AD最大电压高位
                ADcalBuff[5] = (u8)(AD);            //AD最大电压低位
                DAcalBuff[4] = (u8)(DAUcal30 >> 8); //最大电压高位
                DAcalBuff[5] = (u8)(DAUcal30);      //最大电压低位
                if (I2C_BufferWrite(DAcalBuff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal DA Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
                if (I2C_BufferWrite(ADcalBuff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal AD Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
            }
            if (calStage == 2) {
                DelayMs(200);
                AD = bsp_ReadCalAD(1);//读取校准0.1A时的一次电流值
                ADcalBuff[6] = (u8)(AD >> 8);       //电流0.1A高位
                ADcalBuff[7] = (u8)(AD);            //电流0.1A低位
                DAcalBuff[6] = (u8)(DAIcal01 >> 8);
                DAcalBuff[7] = (u8)(DAIcal01);
                if (I2C_BufferWrite(DAcalBuff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal DA Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
                if (I2C_BufferWrite(ADcalBuff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal AD Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
            }
            if (calStage == 3) {
                DelayMs(200);
                AD = bsp_ReadCalAD(1);//读取校准4A时的一次电流值
                ADcalBuff[8] = (u8)(AD >> 8);       //电流4A高位
                ADcalBuff[9] = (u8)(AD);            //电流4A低位
                DAcalBuff[8] = (u8)(DAIcal4 >> 8);
                DAcalBuff[9] = (u8)(DAIcal4);
                if (I2C_BufferWrite(DAcalBuff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal DA Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
                if (I2C_BufferWrite(ADcalBuff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
                    AddNewLog(LOG_LEFT, "Cal AD Saved");
                } else {
                    AddNewLog(LOG_LEFT, "Cal Save Failed");
                }
                DelayMs(100);
            }
            calStage ++;
            if (calStage >= 4) {
                //退出校准时设置电压5V，电流1A
                iSetVoltage = 500;
                iSetCurrent = 1000;
                bsp_SetDacU((float)iSetVoltage / 100);//电压送DAC
                bsp_SetRelay((float)iSetVoltage / 100);
                bsp_SetDacI((float)iSetCurrent / 1000);//电流送DAC
                ShowSetVoltage(iSetVoltage);
                ShowSetCurrent(iSetCurrent);
                break;
            }
            DisplayCalibrateGUI(calStage);//------显示校准菜单
            if (calStage == 0) {
                bsp_SetDacU(0.100);
                bsp_SetRelay(0.100);
                bsp_SetDacI(1.00);
            }//电压0.1V校准时电流设1A
            if (calStage == 1) {
                bsp_SetDacU((float)MAX_VOLTAGE / 100.0);
                bsp_SetRelay((float)MAX_VOLTAGE / 100.0);
                bsp_SetDacI(1.00);//电压30V校准时电流设1A
            }
            if (calStage == 2) {
                bsp_SetDacI(0.100);
                bsp_SetDacU(5.000);//电流0.1A校准时电压设5V
                bsp_SetRelay(5.000);
            }
            if (calStage == 3)    {
                bsp_SetDacI((float)MAX_CURRENT / 1000.0);
                bsp_SetDacU(5.000);//电流最大校准时电压设5V
                bsp_SetRelay(5.000);
            }
        }
        if (stepAddPressed) { //---------步进加
            stepAddPressed = 0;
            if (calStage == 0) {
                DAUcal01 ++;
                if (DAUcal01 > 150) {
                    DAUcal01 = 150;
                }
                DAC_SetChannel1Data(DAC_Align_12b_R, DAUcal01);   //12位电压数据送DAC
            }
            if (calStage == 1) {
                DAUcal30 ++;
                if (DAUcal30 > 4094) {
                    DAUcal30 = 4094;
                }
                DAC_SetChannel1Data(DAC_Align_12b_R, DAUcal30);   //12位电压数据送DAC
            }
            if (calStage == 2) {
                DAIcal01 ++;
                if (DAIcal01 > 400) {
                    DAIcal01 = 400;
                }
                DAC_SetChannel2Data(DAC_Align_12b_R, DAIcal01);   //12位电流数据送DAC
            }
            if (calStage == 3) {
                DAIcal4 ++;
                if (DAIcal4 > 4094) {
                    DAIcal4 = 4094;
                }
                DAC_SetChannel2Data(DAC_Align_12b_R, DAIcal4);    //12位电流数据送DAC
            }
        }
        if (stepSubPressed) { //---------步进减
            stepSubPressed = 0;
            if (calStage == 0) {
                DAUcal01 --;
                if (DAUcal01 > 4094) {
                    DAUcal01 = 0;
                }
                DAC_SetChannel1Data(DAC_Align_12b_R, DAUcal01);   //12位电压数据送DAC
            }
            if (calStage == 1) {
                DAUcal30 --;
                if (DAUcal30 > 4094) {
                    DAUcal30 = 0;
                }
                DAC_SetChannel1Data(DAC_Align_12b_R, DAUcal30);   //12位电压数据送DAC
            }
            if (calStage == 2) {
                DAIcal01 --;
                if (DAIcal01 > 4094) {
                    DAIcal01 = 0;
                }
                DAC_SetChannel2Data(DAC_Align_12b_R, DAIcal01);   //12位电流数据送DAC
            }
            if (calStage == 3) {
                DAIcal4 --;
                if (DAIcal4 > 4094) {
                    DAIcal4 = 0;
                }
                DAC_SetChannel2Data(DAC_Align_12b_R, DAIcal4);    //12位电流数据送DAC
            }
        }
    }
    DAcalBuff[2] = (u8)(DAUcal01 >> 8); //电压0.1V高位
    DAcalBuff[3] = (u8)(DAUcal01);      //电压0.1V低位
    DAcalBuff[4] = (u8)(DAUcal30 >> 8); //电压30V高位
    DAcalBuff[5] = (u8)(DAUcal30);      //电压30V低位
    DAcalBuff[6] = (u8)(DAIcal01 >> 8); //电流0.1A高位
    DAcalBuff[7] = (u8)(DAIcal01);      //电流0.1A高位
    DAcalBuff[8] = (u8)(DAIcal4 >> 8);  //电流4A高位
    DAcalBuff[9] = (u8)(DAIcal4);       //电流4A高位
    DAcalBuff[0]  = 0x55;
    DAcalBuff[1]  = 0xAA;
    ADcalBuff[0]  = 0x55;
    ADcalBuff[1]  = 0xAA;
    if (I2C_BufferWrite(DAcalBuff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
        AddNewLog(LOG_LEFT, "Cal DA Saved");
    } else {
        AddNewLog(LOG_LEFT, "Cal Save Failed");
    }
    if (I2C_BufferWrite(ADcalBuff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
        AddNewLog(LOG_LEFT, "Cal AD Saved");
    } else {
        AddNewLog(LOG_LEFT, "Cal Save Failed");
    }
    //    END_CAL:
    iter = 0;
    FAN_OFF();  //关风机
    I2C_ReadByte(Buff, 10, 0x00, 0xa0); //读取DA电压电流校准后的数据
    if ((Buff[0] == 0x55) && (Buff[1] == 0xAA)) { //假如校准标志正确,就使用校准后的数据
        DAUcal01   = ((u16)Buff[2]) << 8;
        DAUcal01   = ((u16)Buff[3]) | DAUcal01;
        DAUcal30  = ((u16)Buff[4]) << 8;
        DAUcal30  = ((u16)Buff[5]) | DAUcal30;
        DAIcal01  = ((u16)Buff[6]) << 8;
        DAIcal01  = ((u16)Buff[7]) | DAIcal01;
        DAIcal4   = ((u16)Buff[8]) << 8;
        DAIcal4   = ((u16)Buff[9]) | DAIcal4;
    }
    I2C_ReadByte(Buff, 10, 0x20, 0xa0); //读取AD电压电流校准后的数据
    if ((Buff[0] == 0x55) && (Buff[1] == 0xAA)) { //假如校准标志正确,就使用校准后的数据
        ADUcal01   = ((u16)Buff[2]) << 8;
        ADUcal01   = ((u16)Buff[3]) | ADUcal01;
        ADUcal30  = ((u16)Buff[4]) << 8;
        ADUcal30  = ((u16)Buff[5]) | ADUcal30;
        ADIcal01  = ((u16)Buff[6]) << 8;
        ADIcal01  = ((u16)Buff[7]) | ADIcal01;
        ADIcal4   = ((u16)Buff[8]) << 8;
        ADIcal4   = ((u16)Buff[9]) | ADIcal4;
    }
}

/*********************************************************************************
* 函数名称: DispAdj(vu8 AdjustrPointer)
* 功    能: 显示校准
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void DisplayCalibrateGUI(vu8 calStage)
{
    if (calStage == 0) { //当前校准电压0.1V
        ShowSetVoltage(10);//显示电压0.1V
        ShowCalibrate(0);
    }
    if (calStage == 1) { //当前校准电压MAX_VOLTAGE/100
        ShowSetVoltage(MAX_VOLTAGE);//显示电压MAX_VOLTAGE/100V
        ShowCalibrate(0);
    }
    if (calStage == 2) { //当前校准电流0.1A
        ShowSetCurrent(100);//显示电流0.10A
        ShowCalibrate(1);
    }
    if (calStage == 3) { //当前校准电流MAX_CURRENT/1000
        ShowSetCurrent(MAX_CURRENT);//显示电流MAX_CURRENT/1000A
        ShowCalibrate(1);
    }
}


