
#include "bsp_dac.h"

//电压电流的两个校准点的DA值，如果没经过校准就使用此参数，如果已经校准就使用EEPROM里面的参数
vu16   DAUcal01  = 36;
vu16   DAUcal30  = 3972;
vu16   DAIcal01  = 273;//239;
vu16   DAIcal4   = 4051;//4049;//如果万用表的读数小于预设值的话，往大调

vu16  iSetVoltage = 0;//设定的电压值：单位0.01V
vu16  iSetCurrent = 0;//设定的电流值：单位0.001A
/*********************************************************************************
* 函数名称: bsp_InitDAC()
* 功    能: 初始化DAC端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitDAC(void)
{
    GPIO_InitTypeDef      GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(DAC_VOLTAGE_CLK | DAC_CURRENT_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin =  DAC_VOLTAGE_PIN;
    GPIO_Init(DAC_VOLTAGE_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin =  DAC_CURRENT_PIN;
    GPIO_Init(DAC_CURRENT_PORT, & GPIO_InitStructure);
    //设置DAC
    DAC_Config();
}
void bsp_ResetDAC(void)
{
    DAC_SetChannel1Data(DAC_Align_12b_R, 0); //电压DAC清零
    DAC_SetChannel2Data(DAC_Align_12b_R, 0); //电流DAC清零
}

/*********************************************************************************
* 函数名称: bsp_SetDacU()
* 功    能: 输入浮点数电压转换成12位数据送DA
* 参    数: 无
* 返回值  : 无
**********************************************************************************/
vu16 bsp_SetDacU(float x)
{
    float temp;
    u16 i;
    temp = (fMAX_VOLTAGE - 0.10) / ((float)(DAUcal30 - DAUcal01));//计算系数，每LSB电压值
    if (x >= 0.1) { //把低于0.1V电压单独计算
        temp = (x - 0.100) / (float)temp;
        i = (u16)temp + DAUcal01;
    } else {
        temp = (x) / temp;
        i = (u16)temp;
    }
    if (i > 4095) {
        i = 4095;
    }
    DAC_SetChannel1Data(DAC_Align_12b_R, i);   //12位电压数据送DAC
    return i;
}

/*********************************************************************************
* 函数名称: bsp_SetDacI()
* 功    能: 输入浮点数电流转换成12位数据送DA
* 参    数: 无
* 返回值  : 无
**********************************************************************************/
vu16 bsp_SetDacI(float x)
{
    float temp;
    u16 i, a;
    temp = (fMAX_CURRENT - 0.100) / ((float)(DAIcal4 - DAIcal01));
    if (DAIcal01 > ((u16)(0.1 / temp))) {
        a =  DAIcal01 - ((u16)(0.1 / temp));    //计算0电压时候DA值
    } else {
        a = 0;
    }
    temp = ((x) / temp) + a;
    i = (u16)temp;
    if (i > 4095) {
        i = 4095;
    }
    DAC_SetChannel2Data(DAC_Align_12b_R, i);      //12位电流数据送DAC
    return i;
}

/*********************************************************************************
* 函数名称: DAC_Config
* 功    能: DAC配置
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void DAC_Config(void)
{
    DAC_InitTypeDef            DAC_InitStructure;
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_Noise;
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits11_0;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_1, & DAC_InitStructure);
    DAC_Cmd(DAC_Channel_1, ENABLE);
    DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_Noise;
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits11_0;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_2, & DAC_InitStructure);
    DAC_Cmd(DAC_Channel_2, ENABLE);
    DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);
}

