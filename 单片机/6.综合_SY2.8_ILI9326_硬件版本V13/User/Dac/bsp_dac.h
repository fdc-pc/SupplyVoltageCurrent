
#ifndef __BSP_DAC_H
#define __BSP_DAC_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define fMAX_VOLTAGE    30.00
#define fMAX_CURRENT    3.90
#define MAX_VOLTAGE     3000    //单位0.01V
#define MAX_CURRENT     3900    //单位0.001A   

//电压电流的两个校准点的DA值，如果没经过校准就使用此参数，如果已经校准就使用EEPROM里面的参数
extern vu16   DAUcal01;
extern vu16   DAUcal30;
extern vu16   DAIcal01;
extern vu16   DAIcal4;
extern vu16  iSetVoltage;//设定的电压值：单位0.01V
extern vu16  iSetCurrent;//设定的电流值：单位0.001A

//内部调用函数
static void DAC_Config(void);

//外部调用函数
void bsp_InitDAC(void);
void bsp_ResetDAC(void);
vu16 bsp_SetDacU(float x);
vu16 bsp_SetDacI(float x);

#endif
