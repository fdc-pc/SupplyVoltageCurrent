
#include "bsp_output.h"

/*********************************************************************************
* 函数名称: bsp_InitOutput
* 功    能: 初始化输出控制端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitOutput(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(OUTPUT_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;   /* 设为推挽模式 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* IO口最大速度 */
    GPIO_InitStructure.GPIO_Pin = OUTPUT_PIN;
    GPIO_Init(OUTPUT_PORT, & GPIO_InitStructure);
}
void bsp_ResetOutput(void)
{
    //关输出
    OUT_OFF();
}
