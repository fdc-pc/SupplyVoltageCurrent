
#ifndef __BSP_OUTPUT_H
#define __BSP_OUTPUT_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define OUT_ON()        OUTPUT_PORT->BSRR  = OUTPUT_PIN   //�����
#define OUT_OFF()       OUTPUT_PORT->BRR   = OUTPUT_PIN   //����� 

void bsp_InitOutput(void);
void bsp_ResetOutput(void);
#endif
