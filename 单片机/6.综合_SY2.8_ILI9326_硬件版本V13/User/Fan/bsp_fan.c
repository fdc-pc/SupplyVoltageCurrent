
#include "bsp_fan.h"
vu16 TIM4_CCR4_Val = 0;
/*********************************************************************************
* 函数名称: bsp_InitFan()
* 功    能: 初始化风扇端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitFan(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(FAN_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;   // 复用推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin = FAN_PIN;
    GPIO_Init(FAN_PORT, & GPIO_InitStructure);
    //
    //    TIM4_Config();
}
void bsp_ResetFan(void)
{
    FAN_OFF();
}

/*********************************************************************************
* 函数名称: bsp_SetFanSpeed()
* 功    能: 设置风扇速度
* 参    数: 占空比
* 返 回 值: 无
**********************************************************************************/
void bsp_SetFanSpeed(u8 duty)
{
    //if(duty == 0)
    //    TIM_Cmd(TIM4, DISABLE);
    //else
    //{
    if (duty >= 99) {
        duty = 99;
    }
    TIM4_CCR4_Val = duty;
    TIM4->CCR4 = TIM4_CCR4_Val;
    TIM_Cmd(TIM4, ENABLE);
    //}
}

static void TIM4_Config(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    //PWM调速
    TIM_TimeBaseStructure.TIM_Period = 99;
    TIM_TimeBaseStructure.TIM_Prescaler = 35;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;    //设置时钟分频系数：不分频(这里用不到)
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
    TIM_TimeBaseInit(TIM4, & TIM_TimeBaseStructure);
    // PWM1 Mode configuration: Channel1 //
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;   //配置为PWM模式1
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = TIM4_CCR4_Val;          //设置跳变值，当计数器计数到这个值时，电平发生跳变
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;  //当定时器计数值小于CCR1_Val时为高电平
    TIM_OC4Init(TIM4, & TIM_OCInitStructure);    //使能通道1
    TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
}
