
#ifndef __BSP_FAN_H
#define __BSP_FAN_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define    ONFAN            45.00       //超过50度开风机
#define    OFFFAN           40.00       //低于40度关风机
#define    HOTDOWN          80.00       //超过80度进入过热保护

#define FAN_ON()       FAN_PORT->BSRR = FAN_PIN//bsp_SetFanSpeed(99)  //风机开
#define FAN_OFF()      FAN_PORT->BRR  = FAN_PIN//bsp_SetFanSpeed(0)   //风机关

//内部调用
static void TIM4_Config(void);


//外部调用函数
void bsp_InitFan(void);
void bsp_ResetFan(void);
void bsp_SetFanSpeed(u8 duty);
#endif

