
#include "onbutton.h"
#include "bsp_board.h"

/*********************************************************************************
* 函数名称: OnBtnDown()
* 功    能: 按键处理
* 参    数: 无
* 返回值  : 无
**********************************************************************************/
void OnBtnDown(void)
{
    float a;
    u8    Buff[10];
    if (lockBtnPressed) { //按键锁定
        AddNewLog(LOG_LEFT, "Lock Btn");
        lockBtnPressed = 0;
        if (lockKeyState) {
            lockKeyState = 0;
        } else {
            lockKeyState = 1;
        }
    }
    if (ncBtnPressed) { //键2标志
        AddNewLog(LOG_LEFT, "NC Btn");
        ncBtnPressed = 0;
    }
    if (vcBtnPressed) { //电压调节和电流调节切换
        vcBtnPressed = 0;
        if ((stepSign == STEP_01V) || (stepSign == STEP_1V)) {
            stepSign = STEP_01A;
            ShowCurrentStep(10);
            AddNewLog(LOG_LEFT, "V/A Btn: A");
            Gui_DrawFont_HZ32X32(X_VOL, Y_VOL, RED,   GRAY0, "电压");
            Gui_DrawFont_HZ32X32(X_CUR, Y_CUR, GRAY0, BLUE,  "电流");
            DrawVoltageBox(BorderLineColor_V);
            DrawCurrentBox(RED);
        } else {
            stepSign = STEP_01V;
            ShowVoltageStep(1);
            AddNewLog(LOG_LEFT, "V/A Btn: V");
            Gui_DrawFont_HZ32X32(X_VOL, Y_VOL, GRAY0, RED,   "电压");
            Gui_DrawFont_HZ32X32(X_CUR, Y_CUR, BLUE,  GRAY0, "电流");
            DrawVoltageBox(RED);
            DrawCurrentBox(BorderLineColor_A);
        }
    }
    if ((stepAddPressed) && (stepSubPressed)) { //如果两键同时有效
        stepAddPressed = 0;
        stepSubPressed = 0;
    }
    if (stepAddPressed) { //步进加
        stepAddPressed = 0;
        stepSubPressed = 0;
        if (stepSign == STEP_1V) { //电压加1V
            iSetVoltage = iSetVoltage + 100;
            if (iSetVoltage >= MAX_VOLTAGE) {
                iSetVoltage = MAX_VOLTAGE;
            }
            a = (float)iSetVoltage / 100.0;
            bsp_SetDacU(a);
            bsp_SetRelay(a);
            AddNewLog(LOG_LEFT, "V + 1V");
        } else if (stepSign == STEP_01V) { //电压加0.1V
            iSetVoltage = iSetVoltage + 10;
            if (iSetVoltage > MAX_VOLTAGE) {
                iSetVoltage = MAX_VOLTAGE;
            }
            a = (float)iSetVoltage / 100;
            bsp_SetDacU(a);
            bsp_SetRelay(a);
            AddNewLog(LOG_LEFT, "V + 0.1V");
        } else if (stepSign == STEP_01A) { //电流加0.1A
            iSetCurrent = iSetCurrent + 100;
            if (iSetCurrent >= MAX_CURRENT) {
                iSetCurrent = MAX_CURRENT;
            }
            a = ((float)iSetCurrent) / 1000;
            bsp_SetDacI(a);
            AddNewLog(LOG_LEFT, "I + 0.1A");
        } else if (stepSign == STEP_001A) { //电流加0.01A
            iSetCurrent = iSetCurrent + 10;
            if (iSetCurrent > MAX_CURRENT) {
                iSetCurrent = MAX_CURRENT;
            }
            a = ((float)iSetCurrent) / 1000;
            bsp_SetDacI(a);
            AddNewLog(LOG_LEFT, "I + 0.01A");
        }
        Buff[0] = (u8)(iSetVoltage >> 8);      //电压高位
        Buff[1] = (u8)(iSetVoltage);           //电压低位
        Buff[2] = (u8)(iSetCurrent >> 8);      //电流高位
        Buff[3] = (u8)(iSetCurrent);           //电流高位
        if (I2C_BufferWrite(Buff, 4, 0x80, 0xa0)) { //把调节后的数据保存到EEPROM里面
            AddNewLog(LOG_LEFT, "SaveSettedVal");
        } else {
            AddNewLog(LOG_LEFT, "SaveValFailed");
        }
        ShowSetVoltage(iSetVoltage);
        ShowSetCurrent(iSetCurrent);
    }
    if (stepSubPressed) { //步进减
        stepSubPressed = 0;
        stepAddPressed = 0;
        if (stepSign == STEP_1V) { //电压减1V
            if (iSetVoltage >= 100) {
                iSetVoltage = iSetVoltage - 100;
            }
            a = (float)iSetVoltage / 100;
            bsp_SetDacU(a);
            bsp_SetRelay(a);
            AddNewLog(LOG_LEFT, "V - 1V");
        }
        if (stepSign == STEP_01V) { //电压减0.1V
            if (iSetVoltage >= 10) {
                iSetVoltage = iSetVoltage - 10;
            }
            a = (float)iSetVoltage / 100;
            bsp_SetDacU(a);
            bsp_SetRelay(a);
            AddNewLog(LOG_LEFT, "V - 0.1V");
        }
        if (stepSign == STEP_01A) { //电流减0.1A
            if (iSetCurrent >= 100) {
                iSetCurrent = iSetCurrent - 100;
            }
            a = ((float)iSetCurrent) / 1000;
            bsp_SetDacI(a);
            AddNewLog(LOG_LEFT, "I - 0.1A");
        }
        if (stepSign == STEP_001A) { //电流减0.01A
            if (iSetCurrent >= 10) {
                iSetCurrent = iSetCurrent - 10;
            }
            a = ((float)iSetCurrent) / 1000;
            bsp_SetDacI(a);
            AddNewLog(LOG_LEFT, "I - 0.01A");
        }
        Buff[0] = (u8)(iSetVoltage >> 8);      //电压高位
        Buff[1] = (u8)(iSetVoltage);           //电压低位
        Buff[2] = (u8)(iSetCurrent >> 8);      //电流高位
        Buff[3] = (u8)(iSetCurrent);           //电流高位
        if (I2C_BufferWrite(Buff, 4, 0x80, 0xa0)) { //把调节后的数据保存到EEPROM里面
            AddNewLog(LOG_LEFT, "SaveSettedVal");
        } else {
            AddNewLog(LOG_LEFT, "SaveValFailed");
        }
        ShowSetVoltage(iSetVoltage);
        ShowSetCurrent(iSetCurrent);
    }
    if (stepBtnPressed) { //-旋钮按键 切换步长
        AddNewLog(LOG_LEFT, "Step Btn");
        stepBtnPressed = 0;
        if (stepSign == STEP_1V) {
            stepSign = STEP_01V;
            ShowVoltageStep(1);
        } else  if (stepSign == STEP_01V) {
            stepSign = STEP_1V;
            ShowVoltageStep(10);
        } else  if (stepSign == STEP_01A) {
            stepSign = STEP_001A;
            ShowCurrentStep(1);
        } else  if (stepSign == STEP_001A) {
            stepSign = STEP_01A;
            ShowCurrentStep(10);
        }
    }
}

Float2ByteArray tmpFloat2ByteArray2;
u8 tmpSendBuf2[10];
/*********************************************************************************
* 函数名称: TIM3_IRQHandler
* 功    能: TIM3的中断函数，检测按键状态
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void TIM3_IRQHandler(void)
{
    u8 keyCode = 0;
    static u8 iter = 0, iter2 = 0;
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        keyCode = bsp_GetKey();
        if (keyCode > 0) {
            switch (keyCode) {
            case KEY_UUPP_OUTPUT:
                outputBtnPressed = 1;
                break;
            case KEY_LONG_LOCK:
                lockBtnPressed = 1;
                break;
            case KEY_LONG_CAL:
                calBtnPressed = 1;
                break;
            case KEY_UUPP_SWITCH:
                vcBtnPressed = 1;
                break;
            case KEY_UUPP_STEP:
                stepBtnPressed = 1;
                break;
            case KEY_STEP_ADD:
                stepAddPressed = 1;
                break;
            case KEY_STEP_SUB:
                stepSubPressed = 1;
                break;
            }
        }
        if((iter++ == 10)&&(sendDataFlag == 1))
        {
            iter = 0;
            tmpFloat2ByteArray2.fValue = fVoltageGet;
            tmpSendBuf2[0] = USART_S2M_RealVal;
            tmpSendBuf2[1] = tmpFloat2ByteArray2.data[0];
            tmpSendBuf2[2] = tmpFloat2ByteArray2.data[1];
            tmpSendBuf2[3] = tmpFloat2ByteArray2.data[2];
            tmpSendBuf2[4] = tmpFloat2ByteArray2.data[3];
            tmpFloat2ByteArray2.fValue = fCurrentGet;
            tmpSendBuf2[5] = tmpFloat2ByteArray2.data[0];
            tmpSendBuf2[6] = tmpFloat2ByteArray2.data[1];
            tmpSendBuf2[7] = tmpFloat2ByteArray2.data[2];
            tmpSendBuf2[8] = tmpFloat2ByteArray2.data[3];
            bsp_UsartSendFrame((u8*)tmpSendBuf2, 9);            
        }
        if((iter2++ == 200)&&(sendDataFlag == 1))
        {
            iter2 = 0;
            tmpFloat2ByteArray2.fValue = fTempGet;
            tmpSendBuf2[0] = USART_S2M_RealTemp;
            tmpSendBuf2[1] = tmpFloat2ByteArray2.data[0];
            tmpSendBuf2[2] = tmpFloat2ByteArray2.data[1];
            tmpSendBuf2[3] = tmpFloat2ByteArray2.data[2];
            tmpSendBuf2[4] = tmpFloat2ByteArray2.data[3]; 
            bsp_UsartSendFrame((u8*)tmpSendBuf2, 5);                
        }
    }
}
