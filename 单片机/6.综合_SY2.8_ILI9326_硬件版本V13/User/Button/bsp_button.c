
#include "bsp_button.h"

vu16  DELAY_TIME = 0; //延时自减变量
u8    UPDATE_FLAG = 0;
vu8   lockKeyState = 0; //按键锁定标志

vu8   outputBtnPressed = 0; //输出开/关
vu8   lockBtnPressed = 0;   //键盘锁定开关
vu8   ncBtnPressed   = 0;   //空
vu8   calBtnPressed  = 0;   //校正按钮
vu8   vcBtnPressed   = 0;   //V/C切换
vu8   stepAddPressed = 0;   //步进加
vu8   stepSubPressed = 0;   //步进减
vu8   stepBtnPressed = 0;   //切换步进值

STEP_SIGN  stepSign = STEP_1V;//调节标志
//0->电压1V步进
//1->电压0.1V步进
//2->电流0.1A步进
//3->电流0.01步进

static BUTTON_T s_BtnOutput;    //控制输出开关，打开或关闭输出
static BUTTON_T s_BtnLock;      //锁定/解锁面板按键
static BUTTON_T s_BtnNC;        //空
static BUTTON_T s_BtnCal;       //校正开关，长按校正
static BUTTON_T s_BtnSwitch;    //切换电压、电流设置
static BUTTON_T s_BtnStep;      //切换电压步进(1.0V/0.1V)、电流步进(0.1A/0.01A)

static KEY_FIFO_T s_Key;        // 按键FIFO变量,结构体 //

/*********************************************************************************
* 函数名称: bsp_InitButton()
* 功    能: 初始化按键
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitButton(void)
{
    InitButtonVar();        /* 初始化按键变量 */
    InitButtonHard();       /* 初始化按键硬件 */
}
void bsp_ResetButton(void)
{
    SCK_L();
    RCK_L();
    SER_L();
}
/*********************************************************************************
* 函数名称: bsp_GetKey()
* 功    能: 从按键FIFO缓冲区读取一个键值
* 参    数: 无
* 返 回 值: 按键代码
**********************************************************************************/
uint8_t bsp_GetKey(void)
{
    uint8_t ret;
    if (s_Key.Read == s_Key.Write) {
        return KEY_NONE;
    } else {
        ret = s_Key.Buf[s_Key.Read];
        if (++s_Key.Read >= KEY_FIFO_SIZE) {
            s_Key.Read = 0;
        }
        return ret;
    }
}

/*********************************************************************************
* 函数名称: DelayMs
* 功    能: 延时函数
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void DelayMs(u16 x)
{
    DELAY_TIME = x;
    while (DELAY_TIME);
}


/*********************************************************************************
* 函数名称: bsp_InitButtonHard()
* 功    能: 初始化按键硬件，GPIO的设置等
* 参    数: 无
* 返 回 值: 状态
**********************************************************************************/
static void InitButtonHard(void)
{
    Init74HC595();
}

/*********************************************************************************
* 函数名称: bsp_InitButtonVar()
* 功    能: 初始化按键变量
* 参    数: 无
* 返 回 值: 状态
**********************************************************************************/
static void InitButtonVar(void)
{
    //对按键FIFO读写指针清零
    s_Key.Read = 0;
    s_Key.Write = 0;
    //初始化OUTPUT按键
    s_BtnOutput.IsKeyDownFunc = IsKeyDown_BtnOutput;      /* 判断按键按下的函数 */
    s_BtnOutput.FilterTime    = BUTTON_FILTER_TIME;       /* 按键滤波时间 */
    s_BtnOutput.LongTime      = 0;                       /* 长按时间 */
    s_BtnOutput.Count         = s_BtnOutput.FilterTime / 2;/* 计数器设置为滤波时间的一半*/
    s_BtnOutput.State         = 0;                        /* 按键缺省状态，0为未按下 */
    s_BtnOutput.KeyCodeDown   = 0;//KEY_DOWN_OUTPUT;          /* 按键按下的键值代码 */
    s_BtnOutput.KeyCodeUp     = KEY_UUPP_OUTPUT;          /* 按键弹起的键值代码，0表示不检测 */
    s_BtnOutput.KeyCodeLong   = 0;                        /* 按键被持续按下的键值代码，0表示不检测 */
    s_BtnOutput.RepeatSpeed   = 0;                        /* 按键连发的速度，0表示不支持连发，如需連發，可以修改成5 */
    s_BtnOutput.RepeatCount   = 0;                        /* 连发计数器 */
    //锁定按键面板
    s_BtnLock.IsKeyDownFunc = IsKeyDown_BtnLock;       /* 判断按键按下的函数 */
    s_BtnLock.FilterTime    = BUTTON_FILTER_TIME;      /* 按键滤波时间 */
    s_BtnLock.LongTime      = BUTTON_LONG_TIME;        /* 长按时间 */
    s_BtnLock.Count         = s_BtnLock.FilterTime / 2;/* 计数器设置为滤波时间的一半 */
    s_BtnLock.State         = 0;                       /* 按键缺省状态，0为未按下 */
    s_BtnLock.KeyCodeDown   = 0;//KEY_DOWN_LOCK;       /* 按键按下的键值代码 */
    s_BtnLock.KeyCodeUp     = 0;//KEY_UUPP_LOCK;       /* 按键弹起的键值代码，0表示不检测 */
    s_BtnLock.KeyCodeLong   = KEY_LONG_LOCK;                       /* 按键被持续按下的键值代码，0表示不检测 */
    s_BtnLock.RepeatSpeed   = 0;                       /* 按键连发的速度，0表示不支持连发 */
    s_BtnLock.RepeatCount   = 0;                       /* 连发计数器 */
    //空
    s_BtnNC.IsKeyDownFunc = IsKeyDown_BtnNC;         /* 判断按键按下的函数 */
    s_BtnNC.FilterTime    = BUTTON_FILTER_TIME;      /* 按键滤波时间 */
    s_BtnNC.LongTime      = 0;                      /* 长按时间 */
    s_BtnNC.Count         = s_BtnNC.FilterTime / 2;  /* 计数器设置为滤波时间的一半 */
    s_BtnNC.State         = 0;                       /* 按键缺省状态，0为未按下 */
    s_BtnNC.KeyCodeDown   = KEY_DOWN_NC;       /* 按键按下的键值代码 */
    s_BtnNC.KeyCodeUp     = KEY_UUPP_NC;       /* 按键弹起的键值代码，0表示不检测 */
    s_BtnNC.KeyCodeLong   = 0;                       /* 按键被持续按下的键值代码，0表示不检测 */
    s_BtnNC.RepeatSpeed   = 0;                       /* 按键连发的速度，0表示不支持连发 */
    s_BtnNC.RepeatCount   = 0;                       /* 连发计数器 */
    //校准按键：长按校准
    s_BtnCal.IsKeyDownFunc = IsKeyDown_BtnCal;          /* 判断按键按下的函数 */
    s_BtnCal.FilterTime    = BUTTON_FILTER_TIME;        /* 按键滤波时间 */
    s_BtnCal.LongTime      = BUTTON_LONG_TIME;          /* 长按时间 */
    s_BtnCal.Count         = s_BtnCal.FilterTime / 2;   /* 计数器设置为滤波时间的一半 */
    s_BtnCal.State         = 0;                         /* 按键缺省状态，0为未按下 */
    s_BtnCal.KeyCodeDown   = 0;//KEY_DOWN_CAL;              /* 按键按下的键值代码 */
    s_BtnCal.KeyCodeUp     = 0;//KEY_UUPP_CAL;              /* 按键弹起的键值代码，0表示不检测 */
    s_BtnCal.KeyCodeLong   = KEY_LONG_CAL;              /* 按键被持续按下的键值代码，0表示不检测 */
    s_BtnCal.RepeatSpeed   = 0;                         /* 按键连发的速度，0表示不支持连发 */
    s_BtnCal.RepeatCount   = 0;                         /* 连发计数器 */
    //电压/电流切换按钮
    s_BtnSwitch.IsKeyDownFunc = IsKeyDown_BtnSwitch;     /* 判断按键按下的函数 */
    s_BtnSwitch.FilterTime    = BUTTON_FILTER_TIME;      /* 按键滤波时间 */
    s_BtnSwitch.LongTime      = 0;                      /* 长按时间 */
    s_BtnSwitch.Count         = s_BtnSwitch.FilterTime / 2;  /* 计数器设置为滤波时间的一半 */
    s_BtnSwitch.State         = 0;                       /* 按键缺省状态，0为未按下 */
    s_BtnSwitch.KeyCodeDown   = 0;//KEY_DOWN_SWITCH;         /* 按键按下的键值代码 */
    s_BtnSwitch.KeyCodeUp     = KEY_UUPP_SWITCH;         /* 按键弹起的键值代码，0表示不检测 */
    s_BtnSwitch.KeyCodeLong   = 0;                       /* 按键被持续按下的键值代码，0表示不检测 */
    s_BtnSwitch.RepeatSpeed   = 0;                       /* 按键连发的速度，0表示不支持连发 */
    s_BtnSwitch.RepeatCount   = 0;                       /* 连发计数器 */
    //切换电压（1.0V/0.1V）、电流（0.1A/0.01A）步进
    s_BtnStep.IsKeyDownFunc = IsKeyDown_BtnStep;       /* 判断按键按下的函数 */
    s_BtnStep.FilterTime    = BUTTON_FILTER_TIME;      /* 按键滤波时间 */
    s_BtnStep.LongTime      = 0;        /* 长按时间 */
    s_BtnStep.Count         = s_BtnStep.FilterTime / 2;/* 计数器设置为滤波时间的一半 */
    s_BtnStep.State         = 0;                       /* 按键缺省状态，0为未按下 */
    s_BtnStep.KeyCodeDown   = 0;//KEY_DOWN_STEP;           /* 按键按下的键值代码 */
    s_BtnStep.KeyCodeUp     = KEY_UUPP_STEP;           /* 按键弹起的键值代码 */
    s_BtnStep.KeyCodeLong   = 0;           /* 按键被持续按下的键值代码 */
    s_BtnStep.RepeatSpeed   = 0;                       /* 按键连发的速度，0表示不支持连发 */
    s_BtnStep.RepeatCount   = 0;                       /* 连发计数器 */
}

/*********************************************************************************
* 函数名称: IsKeyDown_BtnOutput(), ...
* 功    能: 检测按钮是否按下
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static uint8_t IsKeyDown_BtnOutput(void)
{
    return IsKeyDown(HC595_KEY_OUTPUT);
}
static uint8_t IsKeyDown_BtnLock(void)
{
    return IsKeyDown(HC595_KEY_LOCK);
}
static uint8_t IsKeyDown_BtnNC(void)
{
    return IsKeyDown(HC595_KEY_NC);
}
static uint8_t IsKeyDown_BtnCal(void)
{
    return IsKeyDown(HC595_KEY_CAL);
}
static uint8_t IsKeyDown_BtnSwitch(void)
{
    return IsKeyDown(HC595_KEY_SWITCH);
}
static uint8_t IsKeyDown_BtnStep(void)
{
    return IsKeyDown(HC595_KEY_STEP);
}

/*********************************************************************************
* 函数名称: PutKey()
* 功    能: 将1个键值压入按键FIFO缓冲区。可用于模拟一个按键
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void PutKey(uint8_t _KeyCode)
{
    s_Key.Buf[s_Key.Write] = _KeyCode;
    if (++s_Key.Write >= KEY_FIFO_SIZE) {
        s_Key.Write = 0;
    }
}

///*********************************************************************************
//* 函数名称: bsp_KeyState()
//* 功    能: 读取按键的状态
//* 参    数: 无
//* 返 回 值: 状态
//**********************************************************************************/
//uint8_t bsp_KeyState(uint8_t _ucKeyID)
//{
//    uint8_t ucState = 0;
//    switch (_ucKeyID) {
//    case KID_OUTPUT:
//        ucState = s_BtnOutput.State;
//        break;
//    case KID_LOCK:
//        ucState = s_BtnLock.State;
//        break;
//    case KID_NC:
//        ucState = s_BtnNC.State;
//        break;
//    case KID_CAL:
//        ucState = s_BtnCal.State;
//        break;
//    case KID_SWITCH:
//        ucState = s_BtnSwitch.State;
//        break;
//    case KID_STEP:
//        ucState = s_BtnStep.State;
//        break;
//    }
//    return ucState;
//}

/*********************************************************************************
* 函数名称: DetectButton()
* 功    能: 检测一个按键, 非阻塞状态，必须被周期性的调用
* 参    数: 按键结构变量指针
* 返 回 值: 无
**********************************************************************************/
static void DetectButton(BUTTON_T * _pBtn)
{
    /*
     * 如果没有初始化按键函数，则报错 if (_pBtn->IsKeyDownFunc == 0) {
     * printf("Fault : DetectButton(), _pBtn->IsKeyDownFunc undefine"); }
     */
    if (_pBtn->IsKeyDownFunc()) {
        if (_pBtn->Count < _pBtn->FilterTime) {
            _pBtn->Count = _pBtn->FilterTime;
        } else if (_pBtn->Count < 2 * _pBtn->FilterTime) {
            _pBtn->Count++;
        } else {
            if (_pBtn->State == 0) {
                _pBtn->State = 1;
                /*
                 * 发送按钮按下的消息
                 */
                if (_pBtn->KeyCodeDown > 0) {
                    /*
                     * 键值放入按键FIFO
                     */
                    PutKey(_pBtn->KeyCodeDown);
                }
            }
            if (_pBtn->LongTime > 0) {
                if (_pBtn->LongCount < _pBtn->LongTime) {
                    /*
                     * 发送按钮持续按下的消息
                     */
                    if (++_pBtn->LongCount == _pBtn->LongTime) {
                        /*
                         * 键值放入按键FIFO
                         */
                        PutKey(_pBtn->KeyCodeLong);
                    }
                } else {
                    if (_pBtn->RepeatSpeed > 0) {
                        if (++_pBtn->RepeatCount >= _pBtn->RepeatSpeed) {
                            _pBtn->RepeatCount = 0;
                            /*
                             * 常按键后，每隔10ms发送1个按键
                             */
                            PutKey(_pBtn->KeyCodeDown);
                        }
                    }
                }
            }
        }
    } else {
        if (_pBtn->Count > _pBtn->FilterTime) {
            _pBtn->Count = _pBtn->FilterTime;
        } else if (_pBtn->Count != 0) {
            _pBtn->Count--;
        } else {
            if (_pBtn->State == 1) {
                _pBtn->State = 0;
                /*
                 * 发送按钮弹起的消息
                 */
                if (_pBtn->KeyCodeUp > 0) {
                    /*
                     * 键值放入按键FIFO
                     */
                    PutKey(_pBtn->KeyCodeUp);
                }
            }
        }
        _pBtn->LongCount = 0;
        _pBtn->RepeatCount = 0;
    }
}
/*
 * ****************************************************************************************************
 * 函数名 : KeyPro
 * 描  述 : 检测所有按键。非阻塞状态，必须被周期性的调用
 * 输  入 : 无
 * 输  出 : 无
 * 调  用 : bsp_KeyPro <- SysTick_ISR <- SysTick_Handler
 * ****************************************************************************************************
 */
static void KeyPro(void)
{
    DetectButton( & s_BtnOutput);
    DetectButton( & s_BtnLock);
    //    DetectButton( & s_BtnNC);
    DetectButton( & s_BtnCal);
    DetectButton( & s_BtnSwitch);
    DetectButton( & s_BtnStep);
}

/***************** 74HC595按键检测原理 **********************************
* 当SCK上升沿到来时,SER引脚当前电平值在移位寄存器中左移一位，
* 在下一个上升沿到来时移位寄存器中的所有位都会向左移一位，
* 同时Q7也会串行输出移位寄存器中高位的值，这样连续进行8次，
* 就可以把数组中每一个数（8位的数）送到移位寄存器；
* 然后当RCK上升沿到来时，移位寄存器的值将会被锁存到锁存器里，并从Q1~7引脚输出

* 按钮检测原理：
* 首先 设置SER为输出，串行输入16位(2片级联)数据(每一位对应一个按键，0x01<<keycode)
* 然后 设置SER为输入，检测是否为高电平，如果是则按键按下。
*************************************************************************/

/*
 * ****************************************************************************************************
 * 函数名 : bsp_HC595Tx8, bsp_HC595Tx16
 * 描  述 : 从SER发送1/2字节数据
 * 输  入 : u8/u16 data  待发送的数据
 * 输  出 : 无
 * 调  用 :
 * ****************************************************************************************************
 */
static void HC595Tx8(u8 data)
{
    u8 i;
    for (i = 0; i < 8; i++) {
        if ((data << i) & 0x80) {
            SER_H();
        } else {
            SER_L();
        }
        SCK_L();
        SCK_H();
    }
}
//static void HC595Tx16(u16 data)
//{
//    u8 i;
//    for (i = 0; i < 16; i++) {
//        if ((data << i) & 0x8000) {
//            SER_H();
//        } else {
//            SER_L();
//        }
//        SCK_L();
//        SCK_H();
//    }
//}
/*
 * ****************************************************************************************************
 * 函数名 : SerSetOut, SerSetIn
 * 描  述 : 设置SER为输出/输入
 * 输  入 : 无
 * 输  出 : 无
 * 调  用 :
 * ****************************************************************************************************
 */
static void SerSetOut(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin   = HC595_SER_PIN;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;// 推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(HC595_SER_PORT, & GPIO_InitStructure);
}
static void SerSetIn(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin   = HC595_SER_PIN;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IPD;    //下拉输入
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(HC595_SER_PORT, & GPIO_InitStructure);
}

/*
 * ****************************************************************************************************
 * 函数名 : bsp_Init74HC595
 * 描  述 : 初始化SER, RCK, SCK
 * 输  入 : 无
 * 输  出 : 无
 * 调  用 :
 * ****************************************************************************************************
 */
static void Init74HC595(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(HC595_SER_CLK | HC595_RCK_CLK | HC595_SCK_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin   = HC595_SER_PIN;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(HC595_SER_PORT, & GPIO_InitStructure); /* SER */
    GPIO_InitStructure.GPIO_Pin = HC595_RCK_PIN;
    GPIO_Init(HC595_RCK_PORT, & GPIO_InitStructure); /* RCK */
    GPIO_InitStructure.GPIO_Pin = HC595_SCK_PIN;
    GPIO_Init(HC595_SCK_PORT, & GPIO_InitStructure); /* SCK */
}

/*
 * ****************************************************************************************************
 * 函数名 : IsKeyDown
 * 描  述 : 检测按钮是否按下
 * 输  入 : 按钮编号
 * 输  出 : 0: 未按下; 1:该按键按下
 * 调  用 :
 * ****************************************************************************************************
 */
static u8 IsKeyDown(HC595_KEY key)
{
    SerSetOut();
    HC595Tx8(0x01 << key);
    RCK_H();
    RCK_L();
    SerSetIn();
    if (KEY_IN() == 1) {
        return 1;
    } else {
        return 0;
    }
}

/*********************************************************************************
* 函数名称: TIM2_IRQHandler
* 功    能: TIM2中断响应函数
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void TIM2_IRQHandler(void)
{
    vu8 cha, chb;
    static vu8  iter = 0;
    static vu16 chaDelay = 25;//按键5禁止延时
    static vu16 chbDelay = 25;//按键6禁止延时
    static vu8 stepAddPharse = 0;//按键5状态
    static vu8 stepSubPharse = 0;//按键6状态
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
        if (DELAY_TIME > 0) {
            DELAY_TIME--;
        }
        if (++iter == 200) { //数据刷新频率5Hz
            iter = 0;
            UPDATE_FLAG = 1;
        }
        KeyPro();//按键检测
        //EC11 编码器检测
        cha = IsKeyDown(HC595_KEY_CHA);
        chb = IsKeyDown(HC595_KEY_CHB);
        if (chaDelay > 0) {
            chaDelay --;    //按键5禁止延时
        }
        if (chbDelay > 0) {
            chbDelay --;    //按键6禁止延时
        }
        if ((cha == 0) && (chb == 0) && (stepAddPharse == 0) && (chaDelay == 0)) {
            stepAddPharse = 1;    //cha0, chb0
        } else if ((cha == 0) && (chb == 1) && (stepAddPharse == 1)) {
            stepAddPharse = 2;    //cha0, chb1
        } else if ((cha == 1) && (chb == 1) && (stepAddPharse == 2)) {
            stepAddPharse = 3;    //cha1, chb1
        } else if ((cha == 1) && (chb == 0) && (stepAddPharse == 3)) {
            stepAddPharse = 4;    //cha1, chb0
        } else if ((cha == 0) && (chb == 0) && (stepAddPharse == 4)) {
            stepAddPharse = 0;
            stepSubPharse = 0;
            chbDelay = 25;
            PutKey(KEY_STEP_ADD);//步进加
        } //cha0，chb0编码周期结束
        if ((cha == 0) && (chb == 0) && (stepSubPharse == 0) && (chbDelay == 0)) {
            stepSubPharse = 1;    //cha0，chb0
        } else if ((cha == 1) && (chb == 0) && (stepSubPharse == 1)) {
            stepSubPharse = 2;    //cha1，chb0
        } else if ((cha == 1) && (chb == 1) && (stepSubPharse == 2)) {
            stepSubPharse = 3;    //cha1，chb1
        } else if ((cha == 0) && (chb == 1) && (stepSubPharse == 3)) {
            stepSubPharse = 4;    //chao，chb1
        } else if ((cha == 0) && (chb == 0) && (stepSubPharse == 4)) {
            stepSubPharse = 0;
            stepAddPharse = 0;
            chaDelay = 25;
            PutKey(KEY_STEP_SUB);//步进减
        }//cha0，chb0编码周期结束
        TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
    }
}
