
#ifndef __BSP_BUTTON_H
#define __BSP_BUTTON_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define SER_H() HC595_SER_PORT->BSRR = HC595_SER_PIN;
#define SER_L() HC595_SER_PORT->BRR  = HC595_SER_PIN;
#define RCK_H() HC595_RCK_PORT->BSRR = HC595_RCK_PIN;
#define RCK_L() HC595_RCK_PORT->BRR  = HC595_RCK_PIN;
#define SCK_H() HC595_SCK_PORT->BSRR = HC595_SCK_PIN;
#define SCK_L() HC595_SCK_PORT->BRR  = HC595_SCK_PIN;

#define KEY_IN() (GPIO_ReadInputDataBit(HC595_SER_PORT, HC595_SER_PIN))

//决定各按钮74HC595芯片的偏移量
typedef enum {
    HC595_KEY_OUTPUT  = 0,
    HC595_KEY_LOCK    = 1,
    HC595_KEY_NC      = 2,
    HC595_KEY_CAL     = 3,
    HC595_KEY_SWITCH  = 4,
    HC595_KEY_CHA     = 5,
    HC595_KEY_CHB     = 6,
    HC595_KEY_STEP    = 7,
} HC595_KEY;

/*
 * 按键滤波时间50ms, 单位10ms
 * 只有连续检测到50ms状态不变才认为有效，包括弹起和按下两种事件
 */
#define BUTTON_FILTER_TIME  5
#define BUTTON_LONG_TIME    400     /* 持续1秒，认为长按事件 */

/*
 * 每个按键对应1个全局的结构体变量。
 * 其成员变量是实现滤波和多种按键状态所必须的
 */
typedef struct {
    uint8_t ( * IsKeyDownFunc)(void); /* 按键按下的判断函数,1表示按下 */
    uint8_t Count;                 /* 滤波器计数器 */
    uint8_t FilterTime;            /* 滤波时间(最大255,表示2550ms) */
    uint16_t LongCount;            /* 长按计数器 */
    uint16_t LongTime;             /* 按键按下持续时间, 0表示不检测长按 */
    uint8_t State;                 /* 按键当前状态（按下还是弹起） */
    uint8_t KeyCodeUp;             /* 按键弹起的键值代码, 0表示不检测按键弹起*/
    uint8_t KeyCodeDown;           /* 按键按下的键值代码, 0表示不检测按键按下*/
    uint8_t KeyCodeLong;           /* 按键长按的键值代码, 0表示不检测长按 */
    uint8_t RepeatSpeed;           /* 连续按键周期 */
    uint8_t RepeatCount;           /* 连续按键计数器 */
} BUTTON_T;

//所有按钮的状态，按下/弹起/长按
typedef enum {
    KEY_NONE = 0,

    KEY_DOWN_OUTPUT,
    KEY_UUPP_OUTPUT,

    KEY_DOWN_LOCK,
    KEY_UUPP_LOCK,
    KEY_LONG_LOCK,

    KEY_DOWN_NC,
    KEY_UUPP_NC,

    KEY_DOWN_CAL,
    KEY_UUPP_CAL,
    KEY_LONG_CAL,

    KEY_DOWN_SWITCH,
    KEY_UUPP_SWITCH,

    KEY_DOWN_STEP,
    KEY_UUPP_STEP,

    KEY_STEP_ADD,   //步进加
    KEY_STEP_SUB,   //步进减
}
KEY_ENUM;

enum {
    KID_OUTPUT = 0,
    KID_LOCK,
    KID_NC,
    KID_CAL,
    KID_SWITCH,
    KID_STEP,
    KID_CHA,
    KID_CHB,
};

/*
 * 按键FIFO用到变量
 */
#define KEY_FIFO_SIZE   20
typedef struct {
    uint8_t Buf[KEY_FIFO_SIZE]; /* 键值缓冲区 */
    uint8_t Read;               /* 缓冲区读指针 */
    uint8_t Write;              /* 缓冲区写指针 */
}
KEY_FIFO_T;

typedef enum {
    STEP_01V = 0,
    STEP_1V,
    STEP_001A,
    STEP_01A,
} STEP_SIGN; //指示当前电压/电流步进

extern STEP_SIGN  stepSign;
extern u8    UPDATE_FLAG; //数据更新标示
// extern vu16  displayDelay;//冻结显示设置参数延时
extern vu8   keyEnableFlag;   //键盘使能
extern vu8   lockKeyState; //按键锁定标志
extern vu16  DELAY_TIME;

extern vu8   outputBtnPressed;  //输出开/关
extern vu8   lockBtnPressed;    //键盘锁定开关
extern vu8   ncBtnPressed;      //空
extern vu8   calBtnPressed;     //校正按钮
extern vu8   vcBtnPressed;      //V/C切换
extern vu8   stepAddPressed;    //步进加
extern vu8   stepSubPressed;    //步进减
extern vu8   stepBtnPressed;    //切换步进值

//内部调用
//底层初始化
static void Init74HC595(void);          //初始化74HC595端口
static void SerSetOut(void);            //设置74HC595 SER引脚为输出
static void SerSetIn(void);             //设置74HC595 SER引脚为输入
static u8   IsKeyDown(HC595_KEY key);   //根据偏置检测是否按下
static void HC595Tx8(u8);               //发送一字节数据
static void HC595Tx16(u16);             //发送两字节数据，用于74HC595级联
//上层初始化
static void InitButtonVar(void);            //初始化变量
static void InitButtonHard(void);           //初始化硬件引脚
static void DetectButton(BUTTON_T * _pBtn); //检测按钮
static void PutKey(uint8_t _KeyCode);       //将按键代码存入缓存

static uint8_t IsKeyDown_BtnOutput(void);   //检测Output键是否按下
static uint8_t IsKeyDown_BtnLock(void);     //检测Lock键是否按下
static uint8_t IsKeyDown_BtnNC(void);       //检测NC键是否按下
static uint8_t IsKeyDown_BtnCal(void);      //检测Cal键是否按下
static uint8_t IsKeyDown_BtnSwitch(void);   //检测V/C键是否按下
static uint8_t IsKeyDown_BtnStep(void);     //检测Step键是否按下

static void KeyPro(void);   //定时器中定时调用

//外部调用
void    bsp_InitButton(void);
void    bsp_ResetButton(void);
uint8_t bsp_GetKey(void);
void DelayMs(u16 x);//延时函数
//uint8_t bsp_KeyState(uint8_t _ucKeyID);

#endif
