
#include "oncmd.h"
#include "bsp_board.h"

#define TEST_USART_CALLBACK     0

//u8 TEST_STR[] = {68, 67, 95, 80, 79, 87, 69, 82}; //"1DC_POWER"

Float2ByteArray tmpFloat2ByteArray;
u8 sendDataFlag = 0;
u8 fanCtrlFlag = 0;
u8 tmpSendBuf[10];

void RxCallBack(void * pParam, const u8 * buf, u16 len)
{
#if TEST_USART_CALLBACK
    bsp_UsartSendFrame((u8 *)buf, 5);
#else   //正常
    switch (buf[0]) { //
    case USART_M2S_Output://设置是否输出
        outputBtnPressed = 1;//使能输出
        break;
    case USART_M2S_Mode:
        vcBtnPressed = 1;
        break;
    case USART_M2S_SetVal:
        tmpFloat2ByteArray.data[0] = buf[1];
        tmpFloat2ByteArray.data[1] = buf[2];
        tmpFloat2ByteArray.data[2] = buf[3];
        tmpFloat2ByteArray.data[3] = buf[4];
        iSetVoltage = (u16)(tmpFloat2ByteArray.fValue*100);
        bsp_SetDacU(tmpFloat2ByteArray.fValue);
        bsp_SetRelay(tmpFloat2ByteArray.fValue);
        ShowSetVoltage(iSetVoltage);
    
        tmpFloat2ByteArray.data[0] = buf[5];
        tmpFloat2ByteArray.data[1] = buf[6];
        tmpFloat2ByteArray.data[2] = buf[7];
        tmpFloat2ByteArray.data[3] = buf[8];
        iSetCurrent = (u16)(tmpFloat2ByteArray.fValue*1000);
        bsp_SetDacI(tmpFloat2ByteArray.fValue);
        ShowSetCurrent(iSetCurrent);
    
        AddNewLog(LOG_RIGHT, "USART_CMD_SET_UI");    
        break;
//    case CMD_LOCK:  //设置是否锁定面板按钮
//        lockBtnPressed = 1;
//        break;
    case USART_M2S_Cal:    //校准
        calBtnPressed = 1;
        break;
//    case CMD_STEP:   //步进切换
//        stepBtnPressed = 1;
//        break;
    case USART_M2S_StepAdd: //步进加
        stepAddPressed = 1;
        break;
    case USART_M2S_StepSub: //步进减
        stepSubPressed = 1;
        break;
    case USART_M2S_FAN:
        fanCtrlFlag = buf[1];
        if(buf[1] == 1)
            FAN_ON();
        else if(buf[1] == 0)
            FAN_OFF();
        break;
    case USART_M2S_STATE://控制是否发送数据
        sendDataFlag = buf[1];
        break;
    case USART_M2S_GetVal://返回前一时刻的电压设定值
        tmpFloat2ByteArray.fValue = iSetVoltage / 100.0f;
        tmpSendBuf[0] = USART_S2M_PreVal;
        tmpSendBuf[1] = tmpFloat2ByteArray.data[0];
        tmpSendBuf[2] = tmpFloat2ByteArray.data[1];
        tmpSendBuf[3] = tmpFloat2ByteArray.data[2];
        tmpSendBuf[4] = tmpFloat2ByteArray.data[3];
        tmpFloat2ByteArray.fValue = iSetCurrent / 1000.0f;
        tmpSendBuf[5] = tmpFloat2ByteArray.data[0];
        tmpSendBuf[6] = tmpFloat2ByteArray.data[1];
        tmpSendBuf[7] = tmpFloat2ByteArray.data[2];
        tmpSendBuf[8] = tmpFloat2ByteArray.data[3];
        bsp_UsartSendFrame((u8*)tmpSendBuf, 9);
        AddNewLog(LOG_RIGHT, "USART_CMD_RD_SET");            
        break;
    }
#endif
}



