
#include "stm32f10x.h"
#include "bsp_board.h"
#include "mainbody.h"

int main(void)
{
    u8 iter = 0, bootFlag = 0;
    //设置中断向量表的位置在 0x3000
//    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x3000);
    bsp_InitAllDevice();
    bsp_ResetAllDevice();
    printf("========== DC POWER 0-30V/0-4A ==========\r\n");
    ShowInterface();
    //开机后2s以内按CAL键进入校准模式
    for (iter = 0; iter < 40; iter ++) {
        DelayMs(30);
        if (calBtnPressed) {
            calBtnPressed = 0;
            bootFlag = 1;
            break;
        }
    }
    if (bootFlag == 1) { //开始校正电压/电流
        FAN_ON();
        printf("Start calibrating voltage\r\n");
        printf("voltage  DA        AD\r\n");
        DisplayCalibrateGUI(0);//显示校准0.1V菜单
        CalibrateVoltage(0.10, & ADUcal01, & DAUcal01);
        DisplayCalibrateGUI(1);//显示校准30V菜单
        CalibrateVoltage(fMAX_VOLTAGE, & ADUcal30, & DAUcal30);
        printf("Press CAL to continue calibrating current ...\r\n");
        while (1) {
            if (calBtnPressed) {
                calBtnPressed = 0;
                break;
            }
        }
        printf("Start calibrating current\r\n");
        printf("current  DA        AD\r\n");
        DisplayCalibrateGUI(2);//显示校准0.1A菜单
        CalibrateCurrent(0.10, & ADIcal01, & DAIcal01);
        DisplayCalibrateGUI(3);//显示校准4.0A菜单
        CalibrateCurrent(fMAX_CURRENT, & ADIcal4,  & DAIcal4);
    }
    Start(bootFlag);
    while (1) {
        Run();
        OnBtnDown();
        if (outputBtnPressed) {//Output键控制是否关断输出
            OutPut();
        }
    }
}


/*********************************************END OF FILE**********************/
