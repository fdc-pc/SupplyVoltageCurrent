
#include "bsp_adc.h"
#include "bsp_dac.h"

//电压电流的两个校准点的AD值，如果没经过校准就使用此参数，如果已经校准就使用EEPROM里面的参数
vu16   ADUcal01  = 14;//如果显示的AD值小于万用表读数的话，往小调
vu16   ADUcal30  = 3938;
vu16   ADIcal01  = 248;//213;//如果显示的AD值大于万用表读数的话，往大调
vu16   ADIcal4   = 4018;//4014;//如果显示的AD值小于万用表读数，往小调

//连续采集的ADC_BUFF_SIZE次AD数据缓冲区: [0-15]voltage [16-31]current
vu32 adcVoltageCurrentBuff[ADC_BUFF_SIZE];

vu16 CurrDataCounterBegin = 0, CurrDataCounterEnd = 0;

float fTempGet    = 0.0f; //ADC获取的温度值
float fVoltageGet = 0.0f; //ADC获取的电压值
float fCurrentGet = 0.0f; //ADC获取的电流值

/*********************************************************************************
* 函数名称: bsp_InitADC()
* 功    能: ADC初始化
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitADC(void)
{
    GPIO_InitTypeDef      GPIO_InitStructure;
    //启动GPIO时钟
    RCC_APB2PeriphClockCmd(ADC_VOLTAGE_CLK
                           | ADC_CURRENT_CLK
                           | ADC_TEMPERATURE_CLK
                           , ENABLE);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_InitStructure.GPIO_Pin = ADC_VOLTAGE_PIN;
    GPIO_Init(ADC_VOLTAGE_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = ADC_CURRENT_PIN;
    GPIO_Init(ADC_CURRENT_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = ADC_TEMPERATURE_PIN;
    GPIO_Init(ADC_TEMPERATURE_PORT, & GPIO_InitStructure);
    //配置ADC
    ADC_Config();
}
void bsp_ResetADC(void)
{
    u16 iter;
    for (iter = 0; iter < ADC_BUFF_SIZE; iter++) {
        adcVoltageCurrentBuff[iter] = 0;
    }
}

/*********************************************************************************
* 函数名称: ADC_Config()
* 功    能: ADC设置（包括ADC模块配置和自校准）
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
static void ADC_Config(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    DMA_InitTypeDef DMA_InitStructure;
    /* DMA1 channel1 configuration ----------------------------------------------*/
    DMA_DeInit(DMA1_Channel1);  //将DMA的通道1寄存器重设为缺省值
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)ADC1_DR_Address;  //定义DMA外设基地址
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)adcVoltageCurrentBuff; //定义DMA内存基地址
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;  //外设作为数据传输的来源
    //定义指定DMA通道的DMA缓存的大小，单位为数据单位。根据传输方向，
    //数据单位等于结构中参数DMA_PeripheralDataSize或者参数DMA_MemoryDataSize的值
    DMA_InitStructure.DMA_BufferSize = ADC_BUFF_SIZE;
    //用来设定外设地址寄存器递增与否
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //外设地址寄存器不变
    //DMA_MemoryInc用来设定内存地址寄存器递增与否
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;//内存地址寄存器递增
    //DMA_PeripheralDataSize设定了外设数据宽度
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;//数据宽度为32位
    //DMA_MemoryDataSize设定了外设数据宽度
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word; //数据宽度为32位
    //DMA_Mode设置了CAN的工作模式
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;    //工作在循环缓存模式
    //DMA_Priority设定DMA通道x的软件优先级
    DMA_InitStructure.DMA_Priority = DMA_Priority_High; //DMA通道x拥有高优先级
    //DMA_M2M使能DMA通道的内存到内存传输
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; //DMA通道x没有设置为内存到内存传输
    DMA_Init(DMA1_Channel1, & DMA_InitStructure); //根据DMA_InitStruct中指定的参数初始化DMA的通道x寄存器
    /* Get Current Data Counter value before transfer begins */
    CurrDataCounterBegin = DMA_GetCurrDataCounter(DMA1_Channel1);
    /* Enable DMA1 Channel1 */
    DMA_Cmd(DMA1_Channel1, ENABLE); // 使能或者失能指定的通道x
    /* ADC1 configuration ------------------------------------------------------*/
    ADC_InitStructure.ADC_Mode = ADC_Mode_RegSimult; //ADC1和ADC2工作在同步规则模式
    //ADC_ScanConvMode规定了模数转换工作在扫描模式（多通道）还是单次（单通道）模式。
    //可以设置这个参数为ENABLE或者DISABLE
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    //ADC_ContinuousConvMode规定了模数转换工作在连续还是单次模式。
    //可以设置这个参数为ENABLE或者DISABLE
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    //ADC_ExternalTrigConv定义了使用外部触发来启动规则通道的模数转
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;//转换由软件而不是外部触发启动
    //ADC_DataAlign规定了ADC数据向左边对齐还是向右边对齐
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;//ADC数据右对齐
    //ADC_NbreOfChannel 规定了顺序进行规则转换的ADC通道的数目。这个数目的取值范围是1到16
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    ADC_Init(ADC1, & ADC_InitStructure);
    /* ADC1 regular channels configuration */
    //设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
    //输入参数1 ADCx：x可以是1或者2来选择ADC外设ADC1或ADC2
    //输入参数2 ADC_Channel：被设置的ADC通道
    //输入参数3 Rank：规则组采样顺序。取值范围1到16。
    //输入参数4 ADC_SampleTime：指定ADC通道的采样时间值
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 1, ADC_SampleTime_239Cycles5);
    /* Enable ADC1 DMA */
    ADC_DMACmd(ADC1, ENABLE);
    /* ADC2 configuration ------------------------------------------------------*/
    ADC_InitStructure.ADC_Mode = ADC_Mode_RegSimult;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    ADC_Init(ADC2, & ADC_InitStructure);
    /* ADC2 regular channels configuration */
    ADC_RegularChannelConfig(ADC2, ADC_Channel_13, 1, ADC_SampleTime_239Cycles5);
    /* Enable ADC2 external trigger conversion */
    //使能或者失能ADCx的经外部触发启动注入组转换功能
    ADC_ExternalTrigConvCmd(ADC2, ENABLE);
    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);
    /* Enable Vrefint channel17 */
    ADC_TempSensorVrefintCmd(DISABLE);//使能或者失能温度传感器和内部参考电压通道
    /* Enable ADC1 reset calibaration register */
    ADC_ResetCalibration(ADC1);
    /* Check the end of ADC1 reset calibration register */
    while (ADC_GetResetCalibrationStatus(ADC1));
    /* Start ADC1 calibaration */
    ADC_StartCalibration(ADC1);
    /* Check the end of ADC1 calibration */
    while (ADC_GetCalibrationStatus(ADC1));
    /* Enable ADC2 */
    ADC_Cmd(ADC2, ENABLE);
    /* Enable ADC2 reset calibaration register */
    ADC_ResetCalibration(ADC2);
    /* Check the end of ADC2 reset calibration register */
    while (ADC_GetResetCalibrationStatus(ADC2));
    /* Start ADC2 calibaration */
    ADC_StartCalibration(ADC2);
    /* Check the end of ADC2 calibration */
    while (ADC_GetCalibrationStatus(ADC2));
    /* Start ADC1 Software Conversion */
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);//使能或者失能指定的ADC的软件转换启动功能
    /* Test on Channel 1 DMA_FLAG_TC flag */
    while (!DMA_GetFlagStatus(DMA1_FLAG_TC1));
    /* Clear Channel 1 DMA_FLAG_TC flag */
    DMA_ClearFlag(DMA1_FLAG_TC1);
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;                    //设置ADC工作在独立工作模式
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;                          //设置ADC工作在多通道模式下
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;                    //设置ADC工作在连续工作模式下
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;   //设置ADC工作在软件启动模式下
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;                //设置ADC产生数据右对齐模式
    ADC_InitStructure.ADC_NbrOfChannel = 1;                               //设置ADC顺序转换数目为1
    ADC_Init(ADC3, & ADC_InitStructure);                                  //配置ADC
    /* ADC1 regular channel14 configuration */
    ADC_RegularChannelConfig(ADC3, ADC_Channel_10, 1, ADC_SampleTime_239Cycles5);
    /* Enable ADC3 */
    ADC_Cmd(ADC3, ENABLE);
    /* Enable ADC3 reset calibaration register */
    ADC_ResetCalibration(ADC3);
    /* Check the end of ADC3 reset calibration register */
    while (ADC_GetResetCalibrationStatus(ADC3));
    /* Start ADC3 calibaration */
    ADC_StartCalibration(ADC3);
    /* Check the end of ADC3 calibration */
    while (ADC_GetCalibrationStatus(ADC3));
    /* Start ADC3 Software Conversion */
    ADC_SoftwareStartConvCmd(ADC3, ENABLE);
}

/*********************************************************************************
* 函数名称: bsp_ReadCalAD()
* 功    能: 读一次ad的ADC_BUFF_SIZE次的平均值，返回电压或电流
* 参    数: UI
* 返回值  : 0:电压AD值   1:电流AD值
**********************************************************************************/
extern u8   UPDATE_FLAG;
u16 bsp_ReadCalAD(u8 UI)
{
    vu16 i;
    vu32 U = 0;
    vu32 I = 0;
    //     UPDATE_FLAG = 0;
    //     while (UPDATE_FLAG == 0); //等待更新数据标志
    //     UPDATE_FLAG = 0;
    //     while (UPDATE_FLAG == 0); //再次等待更新数据标志，因为第一次采集可能电压还没稳定建立
    //     UPDATE_FLAG = 0;
    for (i = 0; i < ADC_BUFF_SIZE; i++) { //把32位的合并AD值分离开并累加和
        U += (adcVoltageCurrentBuff[i] & 0x0000FFFF);
        I += (adcVoltageCurrentBuff[i] / 65536);
    }
    U = U / ADC_BUFF_SIZE;
    I = I / ADC_BUFF_SIZE;
    if (UI == 0) {
        return (u16)U;
    } else {
        return (u16)I;
    }
}

/*********************************************************************************
* 函数名称: bsp_GetVoltageCurrentTemp()
* 功    能: 获取当前电压与电流值
* 参    数: 电压、电流
* 返 回 值: 无
**********************************************************************************/
void bsp_GetVoltageCurrentTemp(float * fVoltage, float * fCurrent, float * fTemp)
{
    vu16 i, j;
    float tmp;
    vu32 U = 0;
    vu32 I = 0;
    vu32 T = 0;
    for (i = 0; i < ADC_BUFF_SIZE; i++) { //把32位的合并AD值分离开并累加和
        U += (adcVoltageCurrentBuff[i] & 0x0000FFFF);
        I += (adcVoltageCurrentBuff[i] / 65536);
    }
    //取6000次的平均值
    U = U / ADC_BUFF_SIZE;
    I = I / ADC_BUFF_SIZE;
    if (U >= ADUcal01) {
        tmp = (fMAX_VOLTAGE - 0.10) / (float)(ADUcal30 - ADUcal01);//计算每个LSB的电压值
        tmp = ((float)(U - ADUcal01) * tmp) + 0.10 ;    //计算实际电压值
    } else {
        tmp = 0.10 / (float)ADUcal01;
        tmp = ((float)U * tmp);
        if (tmp <= 0.07) {
            tmp = 0;
        }
        if (tmp > 0.07) {
            tmp = tmp - 0.07;
        }
    }
    if (tmp < 0) {
        tmp = 0;
    }
    * fVoltage = tmp;
    //校正计算电流值
    tmp = (fMAX_CURRENT - 0.100) / (float)(ADIcal4 - ADIcal01);//计算AD值的每个LSB对应的实际电流值
    j = (u16)(0.100 / tmp);//计算0.100A时理论AD值
    //计算AD的0电压时的底数
    if (ADIcal01 > j) { //假如AD读数比理论值偏大
        j = ADIcal01 - j;//计算底数
        if (I > j) {
            tmp = ((float)(I - j) * tmp);
        } else {
            tmp = 0;    //如果读数很小，比计算的底数还小，就修正为0，防止出现5.535读数
        }
    } else {
        j =  j - ADIcal01;//计算负底数
        if (I > 10) {
            tmp = ((float)(I + j) * tmp);
        } else {
            tmp = 0;
        }
    }
    if (tmp < 0) {
        tmp = 0;
    }
    * fCurrent = tmp;
    //读取温度
    T = ADC_GetConversionValue(ADC3);
    tmp = (float)T * 250.0 / 4096.0;
    * fTemp = tmp;
}

