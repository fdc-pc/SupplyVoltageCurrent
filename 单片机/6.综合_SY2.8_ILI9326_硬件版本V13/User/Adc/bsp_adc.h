
#ifndef __BSP_ADC_H
#define __BSP_ADC_H

#include "stm32f10x.h"
#include "GpioConfig.h"

#define     ADC1_DR_Address    ((u32)0x4001244C)
#define     ADC_BUFF_SIZE    6000 //ADC数组大小

typedef union {
    u16 value;
    u8  data[4];
} U16_To_BYTE;
typedef union {
    float fValue;
    u8    data[4];
}Float2ByteArray;


extern vu16   ADUcal01;
extern vu16   ADUcal30;
extern vu16   ADIcal01;
extern vu16   ADIcal4;
extern vu32 adcVoltageCurrentBuff[];

extern float fTempGet;    //ADC获取的温度值
extern float fVoltageGet; //ADC获取的电压值
extern float fCurrentGet; //ADC获取的电流值

//内部调用
static void ADC_Config(void);

//外部调用
void bsp_InitADC(void);
void bsp_ResetADC(void);
void bsp_GetVoltageCurrentTemp(float * fVoltage, float * fCurrent, float * fTemp);
u16 bsp_ReadCalAD(u8 UI);

#endif
