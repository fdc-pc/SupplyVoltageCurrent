
#ifndef __BSP_I2C_H
#define __BSP_I2C_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define SCL_H         I2C_SCL_PORT->BSRR = I2C_SCL_PIN
#define SCL_L         I2C_SCL_PORT->BRR  = I2C_SCL_PIN

#define SDA_H         I2C_SDA_PORT->BSRR = I2C_SDA_PIN
#define SDA_L         I2C_SDA_PORT->BRR  = I2C_SDA_PIN

#define WP_L()        I2C_WP_PORT->BRR  = I2C_WP_PIN  //写使能
#define WP_H()        I2C_WP_PORT->BSRR = I2C_WP_PIN  //写失能

#define SCL_read      I2C_SCL_PORT->IDR  & I2C_SCL_PIN
#define SDA_read      I2C_SDA_PORT->IDR  & I2C_SDA_PIN

#define I2C_PageSize  8  //24C02每页8字节

#ifndef bool
typedef enum {FALSE = 0, TRUE = !FALSE} bool;
#endif


//内部调用函数
static void I2C_delay(void);
static bool I2C_Start(void);
static void I2C_Stop(void);
static void I2C_Ack(void);
static void I2C_NoAck(void);
static bool I2C_WaitAck(void);
static void I2C_SendByte(u8 SendByte);
static u8 I2C_ReceiveByte(void);
static bool I2C_WriteByte(u8 SendByte, u16 WriteAddress, u8 DeviceAddress);

//外部调用函数
void bsp_InitI2C(void);
void bsp_ResetI2C(void);
bool I2C_BufferWrite(u8 * pBuffer, u8 length,     u16 WriteAddress, u8 DeviceAddress);
bool I2C_ReadByte(u8 * pBuffer,   u8 length,     u16 ReadAddress,  u8 DeviceAddress);


/*
EEPROM地址分配
DA校准：
0x00 - 0x01         0x55 0xAA        校准数据有效标志，如果不是这两个数，那就默认初始校准数据
0x02                //电压0.1V高位
0x03                //电压0.1V低位
0x04                //电压30V高位
0x05                //电压30V低位
0x06                //电流0.1A高位
0x07                //电流0.1A高位
0x08                //电流4A高位
0x09                //电流4A高位

AD校准：
0x20 - 0x21         0x55 0xAA        校准数据有效标志，如果不是这两个数，那就默认初始校准数据
0x22                //电压0.1V高位
0x23                //电压0.1V低位
0x24                //电压30V高位
0x25                //电压30V低位
0x26                //电流0.1A高位
0x27                //电流0.1A高位
0x28                //电流4A高位
0x29                //电流4A高位


随机电压电流：
0x80                //上次关机时电压高位
0x81                //上次关机时电压低位
0x82                //上次关机时电流高位
0x83                //上次关机时电流高位
*/

#endif
