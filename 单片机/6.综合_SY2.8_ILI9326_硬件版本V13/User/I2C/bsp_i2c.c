#include "bsp_i2c.h"
/********************************************************/
extern void DelayMs(u16 x);   //显示扫描中断计数延时

/*********************************************************************************
* 函数名称: bsp_InitI2C(), void bsp_ResetI2C(void)
* 功    能: 初始化I2C端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitI2C(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(I2C_SCL_CLK | I2C_SDA_CLK | I2C_WP_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* IO口最大速度 */
    GPIO_InitStructure.GPIO_Pin = I2C_SCL_PIN;
    GPIO_Init(I2C_SCL_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = I2C_SDA_PIN;
    GPIO_Init(I2C_SDA_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;   /* 设为推挽模式 */
    GPIO_InitStructure.GPIO_Pin = I2C_WP_PIN;
    GPIO_Init(I2C_WP_PORT, & GPIO_InitStructure);
}
void bsp_ResetI2C(void)
{
    WP_H();//禁止写
}


/*********************************************************************************
* 函数名称: I2C_BufferWrite
* 功    能: 写入1串数据, 注意不能跨页写
* 参    数: 待写入数组地址    待写入长度      待写入地址     器件类型(24c16或SD2403)
* 返 回 值: 无
**********************************************************************************/
bool I2C_BufferWrite(u8 * pBuffer, u8 length, u16 WriteAddress, u8 DeviceAddress)
{
    //u16 i;
    WP_L();
    if (!I2C_Start()) {
        return FALSE;
    }
    I2C_SendByte(((WriteAddress & 0x0700) >> 7) | DeviceAddress & 0xFFFE); //设置高起始地址+器件地址
    if (!I2C_WaitAck()) {
        I2C_Stop();
        return FALSE;
    }
    I2C_SendByte((u8)(WriteAddress & 0x00FF));   //设置低起始地址
    I2C_WaitAck();
    while (length--) {
        I2C_SendByte( * pBuffer);
        I2C_WaitAck();
        pBuffer++;
    }
    I2C_Stop();
    //注意：因为这里要等待EEPROM写完，可以采用查询或延时方式(10ms)
    DelayMs(10);
    WP_H();
    return TRUE;
}


/*********************************************************************************
* 函数名称: I2C_PageWrite
* 功    能: 跨页写入1串数据
* 参    数: 待写入数组地址    待写入长度      待写入地址     器件类型(24c16或SD2403)
* 返 回 值: 无
**********************************************************************************/
void I2C_PageWrite(u8 * pBuffer, u8 length,     u16 WriteAddress, u8 DeviceAddress)
{
    u8 NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0;
    u16  i;
    WP_L();
    Addr  = WriteAddress % I2C_PageSize;      //写入地址是开始页的第几位
    count = I2C_PageSize - Addr;                        //在开始页要写入的个数
    NumOfPage   =  length / I2C_PageSize;     //要写入的页数
    NumOfSingle =  length % I2C_PageSize;     //不足一页的个数
    if (Addr == 0) {      //写入地址是页的开始
        if (NumOfPage == 0) { //数据小于一页
            I2C_BufferWrite(pBuffer, NumOfSingle, WriteAddress, DeviceAddress); //写少于一页的数据
        } else {              //数据大于等于一页
            while (NumOfPage) { //要写入的页数
                I2C_BufferWrite(pBuffer, I2C_PageSize, WriteAddress, DeviceAddress); //写一页的数据
                WriteAddress +=  I2C_PageSize;
                pBuffer      +=  I2C_PageSize;
                NumOfPage--;
                DelayMs(10);
            }
            if (NumOfSingle != 0) { //剩余数据小于一页
                I2C_BufferWrite(pBuffer, NumOfSingle, WriteAddress, DeviceAddress); //写少于一页的数据
                DelayMs(10);
            }
        }
    } else {              //写入地址不是页的开始
        if (NumOfPage == 0) { //数据小于一页
            I2C_BufferWrite(pBuffer, NumOfSingle, WriteAddress, DeviceAddress); //写少于一页的数据
        } else {            //数据大于等于一页
            length       -= count;
            NumOfPage     = length / I2C_PageSize;  //重新计算要写入的页数
            NumOfSingle   = length % I2C_PageSize;  //重新计算不足一页的个数
            if (count != 0) {
                I2C_BufferWrite(pBuffer, count, WriteAddress, DeviceAddress);   //将开始的空间写满一页
                WriteAddress += count;
                pBuffer      += count;
            }
            while (NumOfPage--) { //要写入的页数
                I2C_BufferWrite(pBuffer, I2C_PageSize, WriteAddress, DeviceAddress); //写一页的数据
                WriteAddress +=  I2C_PageSize;
                pBuffer      +=  I2C_PageSize;
            }
            if (NumOfSingle != 0) { //剩余数据小于一页
                I2C_BufferWrite(pBuffer, NumOfSingle, WriteAddress, DeviceAddress); //写少于一页的数据
            }
        }
    }
    for (i = 0; i < 1500; i ++);
    WP_H();
}

/*********************************************************************************
* 函数名称: I2C_ReadByte
* 功    能: 读出1串数据
* 参    数: 存放读出数据  待读出长度      待读出地址       器件类型(24c16或SD2403)
* 返 回 值: 无
**********************************************************************************/
bool I2C_ReadByte(u8 * pBuffer,   u8 length,     u16 ReadAddress,  u8 DeviceAddress)
{
    if (!I2C_Start()) {
        return FALSE;
    }
    I2C_SendByte(((ReadAddress & 0x0700) >> 7) | DeviceAddress & 0xFFFE); //设置高起始地址+器件地址
    if (!I2C_WaitAck()) {
        I2C_Stop();
        return FALSE;
    }
    I2C_SendByte((u8)(ReadAddress & 0x00FF));   //设置低起始地址
    I2C_WaitAck();
    I2C_Start();
    I2C_SendByte(((ReadAddress & 0x0700) >> 7) | DeviceAddress | 0x0001);
    I2C_WaitAck();
    while (length) {
        * pBuffer = I2C_ReceiveByte();
        if (length == 1) {
            I2C_NoAck();
        } else {
            I2C_Ack();
        }
        pBuffer++;
        length--;
    }
    I2C_Stop();
    return TRUE;
}

//========================================================================================//
//========================================================================================//
static void I2C_delay(void)
{
    u16 i = 300; //这里可以优化速度  ，经测试最低到5还能写入
    while (i) {
        i--;
    }
}
static bool I2C_Start(void)
{
    SDA_H;
    SCL_H;
    I2C_delay();
    if (!SDA_read) {
        return FALSE;    //SDA线为低电平则总线忙,退出
    }
    SDA_L;
    I2C_delay();
    if (SDA_read) {
        return FALSE;    //SDA线为高电平则总线出错,退出
    }
    SDA_L;
    I2C_delay();
    return TRUE;
}

static void I2C_Stop(void)
{
    SCL_L;
    I2C_delay();
    SDA_L;
    I2C_delay();
    SCL_H;
    I2C_delay();
    SDA_H;
    I2C_delay();
}

static void I2C_Ack(void)
{
    SCL_L;
    I2C_delay();
    SDA_L;
    I2C_delay();
    SCL_H;
    I2C_delay();
    SCL_L;
    I2C_delay();
}

static void I2C_NoAck(void)
{
    SCL_L;
    I2C_delay();
    SDA_H;
    I2C_delay();
    SCL_H;
    I2C_delay();
    SCL_L;
    I2C_delay();
}

static bool I2C_WaitAck(void)   //返回为:=1有ACK,=0无ACK
{
    SCL_L;
    I2C_delay();
    SDA_H;
    I2C_delay();
    SCL_H;
    I2C_delay();
    if (SDA_read) {
        SCL_L;
        return FALSE;
    }
    SCL_L;
    return TRUE;
}

static void I2C_SendByte(u8 SendByte) //数据从高位到低位//
{
    u8 i = 8;
    while (i--) {
        SCL_L;
        I2C_delay();
        if (SendByte & 0x80) {
            SDA_H;
        } else {
            SDA_L;
        }
        SendByte <<= 1;
        I2C_delay();
        SCL_H;
        I2C_delay();
    }
    SCL_L;
}

static u8 I2C_ReceiveByte(void)  //数据从高位到低位//
{
    u8 i = 8;
    u8 ReceiveByte = 0;
    SDA_H;
    while (i--) {
        ReceiveByte <<= 1;
        SCL_L;
        I2C_delay();
        SCL_H;
        I2C_delay();
        if (SDA_read) {
            ReceiveByte |= 0x01;
        }
    }
    SCL_L;
    return ReceiveByte;
}

//写入1字节数据       待写入数据    待写入地址       器件类型(24c16或SD2403)
static bool I2C_WriteByte(u8 SendByte, u16 WriteAddress, u8 DeviceAddress)
{
    WP_L();
    if (!I2C_Start()) {
        return FALSE;
    }
    I2C_SendByte(((WriteAddress & 0x0700) >> 7) | DeviceAddress & 0xFFFE); //设置高起始地址+器件地址
    if (!I2C_WaitAck()) {
        I2C_Stop();
        return FALSE;
    }
    I2C_SendByte((u8)(WriteAddress & 0x00FF));   //设置低起始地址
    I2C_WaitAck();
    I2C_SendByte(SendByte);
    I2C_WaitAck();
    I2C_Stop();
    //注意：因为这里要等待EEPROM写完，可以采用查询或延时方式(10ms)
    //Systick_Delay_1ms(10);
    WP_H();
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

