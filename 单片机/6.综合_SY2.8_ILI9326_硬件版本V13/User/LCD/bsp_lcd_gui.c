
#include <string.h>
#include "bsp_lcd_gui.h"
#include "font.h"


unsigned char voltageSetArray[] = {0, 0, 10, 0, 0, 14, SEG_END_SIGN};
unsigned char currentSetArray[] = {0, 10, 0, 0, 0, 15, SEG_END_SIGN};
unsigned char voltageStep[]  = {1, 10, 0, 0, 14, SEG_END_SIGN};
unsigned char currentStep[]  = {0, 10, 1, 0, 15, SEG_END_SIGN};
unsigned char temprature[]   = {0, 0, 10, 0, 13, SEG_END_SIGN};
unsigned char voltageGetArray[] = {0, 0, 10, 0, 0, 14, SEG_END_SIGN};
unsigned char currentGetArray[] = {0, 10, 0, 0, 0, 15, SEG_END_SIGN};

unsigned char calArray[] = {16, 15, CHAR_LEN, SEG_END_SIGN};//显示"CAL"

char prevLog[CHAR_LEN] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};//13个空格
char currentLog[CHAR_LEN] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
char logArrayL[LINE_LEN][CHAR_LEN + 1];//左半边Log信息
char logArrayR[LINE_LEN][CHAR_LEN + 1];//右半边Log信息

/****************************************************************************
* 名    称：void DrawVoltageBox(u16 color), void DrawCurrentBox(u16 color)
* 功    能：画边框
* 入口参数：无
* 出口参数：无
* 说    明：
****************************************************************************/
void DrawVoltageBox(u16 color)
{
    Gui_Fill_Rec(0,   0,   239, 6,   color);
    Gui_Fill_Rec(0,   0,   6,   135, color);
    Gui_Fill_Rec(0,   130, 239, 135, color);
    Gui_Fill_Rec(234, 0,   239, 135, color);
}
void DrawCurrentBox(u16 color)
{
    Gui_Fill_Rec(0,   136, 239,  141, color);
    Gui_Fill_Rec(0,   136,   6,  271, color);
    Gui_Fill_Rec(0,   266, 239,  271, color);
    Gui_Fill_Rec(234, 136, 239,  271, color);
}

/****************************************************************************
* 名    称：void ShowInterface(void), ShowCalibrate
* 功    能：显示界面
* 入口参数：无
* 出口参数：无
* 说    明：
****************************************************************************/
void ShowInterface(void)
{
    u8 i, j;
    Lcd_Clear(WHITE);
    DrawVoltageBox(RED);
    Gui_DrawFont_HZ32X32(X_VOL,       Y_VOL,     GRAY0,    RED,   "电压");//反显
    Gui_DrawFont_Num32X24(X_VOL_STEP, Y_VOL,     RED,      GRAY0, & voltageStep[0]);
    Gui_DrawFont_HZ32X32(X_VOL,       Y_VOL_SET, ROSE,     GRAY0, "设定");
    Gui_DrawFont_Num32X24(X_VOL_SET,  Y_VOL_SET, ROSE,     GRAY0, & voltageSetArray[0]);
    Gui_DrawFont_HZ32X32(X_VOL,       Y_VOL_GET, RED,      GRAY0, "实际");
    Gui_DrawFont_Num32X24(X_VOL_GET,  Y_VOL_GET, RED,      GRAY0, & voltageGetArray[0]); //实际电压值
    DrawCurrentBox(BorderLineColor_A);
    Gui_DrawFont_HZ32X32(X_CUR,       Y_CUR,     BLUE,     GRAY0, "电流");
    Gui_DrawFont_Num32X24(X_CUR_STEP, Y_CUR,     BLUE,     GRAY0, & currentStep[0]);
    Gui_DrawFont_HZ32X32(X_CUR,       Y_CUR_SET, DARK_BLUE, GRAY0, "设定");
    Gui_DrawFont_Num32X24(X_CUR_SET,  Y_CUR_SET, DARK_BLUE, GRAY0, & currentSetArray[0]);
    Gui_DrawFont_HZ32X32(X_CUR,       Y_CUR_GET, BLUE,     GRAY0, "实际");
    Gui_DrawFont_Num32X24(X_CUR_GET,  Y_CUR_GET, BLUE,     GRAY0, & currentGetArray[0]); //实际电流值
    Gui_DrawFont_HZ32X32(X_TEMP,      Y_TEMP, BLACK,     GRAY0, "温度");
    Gui_DrawFont_Num32X24(X_TEMP_GET, Y_TEMP, BLACK,     GRAY0, & temprature[0]);
    //log区域：左半边0-111显示按钮log；右半边128-239显示串口信息
    Gui_Fill_Rec(0,   Y_LOG, 239, 399, GRAY1);
    for (i = 0; i < LINE_LEN; i++) {
        for (j = 0; j < CHAR_LEN; j++) {
            logArrayL[i][j] = ' ';
            logArrayR[i][j] = ' ';
        }
        logArrayL[i][CHAR_LEN] = '\0';
        logArrayR[i][CHAR_LEN] = '\0';
    }
}
void ShowCalibrate(u8 voltageOrCurrent)
{
    if (voltageOrCurrent == 0) { //校准电压
        Gui_DrawFont_HZ32X32(X_VOL,   Y_VOL_SET, ROSE,     GRAY0, "校准");
    } else { //正在校准电流
        Gui_DrawFont_HZ32X32(X_CUR,   Y_CUR_SET, DARK_BLUE, GRAY0, "校准");
    }
}

/****************************************************************************
* 名    称：void ShowVoltageStep(void), ShowCurrentStep, ...
* 功    能：显示相应的数据
* 入口参数：无
* 出口参数：无
* 说    明：
****************************************************************************/
void ShowVoltageStep(u8 iStep)
{
    voltageStep[2] = iStep % 10;
    iStep /= 10;
    voltageStep[0] = iStep % 10;
    Gui_DrawFont_Num32X24(X_VOL_STEP, Y_VOL, RED,      GRAY0, & voltageStep[0]);
}
void ShowCurrentStep(u8 iStep)
{
    currentStep[3] = iStep % 10;
    iStep /= 10;
    currentStep[2] = iStep % 10;
    Gui_DrawFont_Num32X24(X_CUR_STEP, Y_CUR, BLUE,     GRAY0, & currentStep[0]);
}
void ShowSetVoltage(u16 iVoltage)
{
    voltageSetArray[4] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageSetArray[3] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageSetArray[1] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageSetArray[0] = (u8)(iVoltage % 10);
    Gui_DrawFont_Num32X24(X_VOL_SET,  Y_VOL_SET, ROSE,     GRAY0, & voltageSetArray[0]);
}
void ShowGetVoltage(float fVoltage)
{
    u16 iVoltage = (u16)(fVoltage * 100);
    voltageGetArray[4] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageGetArray[3] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageGetArray[1] = (u8)(iVoltage % 10);
    iVoltage /= 10;
    voltageGetArray[0] = (u8)(iVoltage % 10);
    Gui_DrawFont_Num32X24(X_VOL_GET,  Y_VOL_GET, RED,      GRAY0, & voltageGetArray[0]); //实际电压值
}
void ShowSetCurrent(u16 iCurrent)
{
    currentSetArray[4] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentSetArray[3] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentSetArray[2] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentSetArray[0] = (u8)(iCurrent % 10);
    Gui_DrawFont_Num32X24(X_CUR_SET,  Y_CUR_SET, DARK_BLUE, GRAY0, & currentSetArray[0]);
}
void ShowGetCurrent(float fCurrent)
{
    u16 iCurrent = (u16)(fCurrent * 1000);
    currentGetArray[4] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentGetArray[3] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentGetArray[2] = (u8)(iCurrent % 10);
    iCurrent /= 10;
    currentGetArray[0] = (u8)(iCurrent % 10);
    Gui_DrawFont_Num32X24(X_CUR_GET,  Y_CUR_GET, BLUE,     GRAY0, & currentGetArray[0]); //实际电流值
}
void ShowGetTemperature(float fTemp)
{
    u16 iTemp = (u16)(fTemp * 10);
    temprature[3] = (u8)(iTemp % 10);
    iTemp /= 10;
    temprature[1] = iTemp % 10;
    iTemp /= 10;
    temprature[0] = iTemp % 10;
    Gui_DrawFont_Num32X24(X_TEMP_GET, Y_TEMP, BLACK,     GRAY0, & temprature[0]);
}

/****************************************************************************
* 名    称：void Gui_DrawFont_Num32X24(u16 x, u16 y, u16 fc, u16 bc, u16 num)
* 功    能：数码管字体
* 入口参数：xy坐标，fc字体颜色, bc背景颜色, num字符编号
* 出口参数：无
* 说    明：
****************************************************************************/
void Gui_DrawFont_Num32X24(u16 x, u16 y, u16 fc, u16 bc, u8 * num)
{
    u8  row = 0, //行号
        tmp, i, j,
        ithChar = 0; //第几个字符
    while ( * num != SEG_END_SIGN) {
        for (row = 0; row < 32; row++) {
            LCD_SetCursor(x + ithChar * 24, y + row); //设置光标位置
            LCD_WriteRAM_Prepare(); //开始写入GRAM
            for (i = 0; i < 3; i++) {
                tmp = * (NUM24X32_DIGITRON + * num * 96 + 3 * row + i);
                for (j = 0; j < 8; j++) {
                    if ((tmp & 0x80) == 0x80) {
                        LCD_WR_DATA(fc);
                    } else {
                        LCD_WR_DATA(bc);
                    }
                    tmp <<= 1;
                }
            }
        }
        num++;
        ithChar++;
    }
}

/****************************************************************************
* 名    称：void Gui_DrawFont_HZ32X32(u16 x, u16 y, u16 fc, u16 bc, char * s)
* 功    能：汉字显示
* 入口参数：xy坐标，fc字体颜色, bc背景颜色, num字符编号
* 出口参数：无
* 说    明：
****************************************************************************/
void Gui_DrawFont_HZ32X32(u16 x, u16 y, u16 fc, u16 bc, char * s)
{
    u8  i = 0, j = 0, row = 0, k = 0, tmp = 0, ithChar = 0;
    while ( * s) {
        for (k = 0; k < HZ32_NUM; k++) {
            if ((HZ32_SONG[k].Index[0] == * (s)) && (HZ32_SONG[k].Index[1] == * (s + 1))) {
                for (row = 0; row < 32; row++) {
                    LCD_SetCursor(x + ithChar * 32, y + row); //设置光标位置
                    LCD_WriteRAM_Prepare();    //开始写入GRAM
                    for (i = 0; i < 4; i++) {
                        tmp = HZ32_SONG[k].Msk[4 * row + i];
                        for (j = 0; j < 8; j++) {
                            if ((tmp & 0x80) == 0x80) {
                                LCD_WR_DATA(fc);
                            } else {
                                LCD_WR_DATA(bc);
                            }
                            tmp <<= 1;
                        }
                    }
                }
            }
        }
        s = s + 2;
        ithChar++;
    }
}

/****************************************************************************
* 名    称：void Gui_DrawFont_ASC16X08(u16 x, u16 y, u16 fc, u16 bc, char * s)
* 功    能：ASCII字符显示
* 入口参数：xy坐标，fc字体颜色, bc背景颜色, num字符编号
* 出口参数：无
* 说    明：
****************************************************************************/
void Gui_DrawFont_ASC16X08(u16 x, u16 y, u16 fc, u16 bc, char * s)
{
    u8 j = 0, k = 0, ithChar = 0, tmp = 0, row = 0;
    //    u16 x0;
    //    x0 = x;
    while ( * s) {
        if (( * s) < 128) {
            k = * s;
            if (k == 13) { //回车
#ifdef  H_VIEW
                //                if (H_VIEW == 1) {
                //                    x0 += 16;
                //                }
#endif
            } else {
                if (k > 32) { //根据ASCII表，前32位为控制字符，不显示
                    k -= 32;
                } else {
                    k = 0;
                }
                for (row = 0; row < 16; row++) {
                    LCD_SetCursor(x + ithChar * 8, y + row); //设置光标位置
                    LCD_WriteRAM_Prepare();    //开始写入GRAM
                    tmp = asc16[k * 16 + row];
                    for (j = 0; j < 8; j++) {
                        if ((tmp & 0x80) == 0x80) {
                            LCD_WR_DATA(fc);
                        } else {
                            LCD_WR_DATA(bc);
                        }
                        tmp <<= 1;
                    }
                }
                ithChar++;
            }
            s ++;
        }
    }
}

/****************************************************************************
* 名    称：void Gui_DrawFont_ASC24X16(u16 x, u16 y, u16 fc, u16 bc, char * s)
* 功    能：大号ASCII字符显示
* 入口参数：xy坐标，fc字体颜色, bc背景颜色, num字符编号
* 出口参数：无
* 说    明：
****************************************************************************/
void Gui_DrawFont_ASC24X16(u16 x, u16 y, u16 fc, u16 bc, char * s)
{
    u8 i = 0, j = 0, k = 0, ithChar = 0, row = 0, tmp = 0;
    //    u16 x0 = 0;
    //    x0 = x;
    while ( * s) {
        if (( * s) < 128) { //ASCII字符
            k = * s;
            if (k == 13) { //回车
#ifdef  H_VIEW
                //                if (H_VIEW == 1) {
                //                    x0 += 16;
                //                }
#endif
            } else {
                if (k > 32) { //根据ASCII表，前32位为控制字符，不显示
                    k -= 32;
                } else {
                    k = 0;
                }
                for (row = 0; row < 24; row++) {
                    LCD_SetCursor(x + ithChar * 16, y + row); //设置光标位置
                    LCD_WriteRAM_Prepare();    //开始写入GRAM
                    for (i = 0; i < 2; i++) {
                        tmp = ASCII_16x24[k * 48 + row * 2 + i];
                        for (j = 0; j < 8; j++) {
                            if ((tmp & 0x80) == 0x80) {
                                LCD_WR_DATA(fc);
                            } else {
                                LCD_WR_DATA(bc);
                            }
                            tmp <<= 1;
                        }
                    }
                }
                ithChar++;
            }
            s ++;
        }
    }
}

/****************************************************************************
* 名    称：void AddNewLog(LOG_SIDE leftOrRight, const char * log)
* 功    能：在左侧/右侧LOG区显示信息
* 入口参数：左侧/右侧    信息内容
* 出口参数：无
* 说    明：
****************************************************************************/
void AddNewLog(LOG_SIDE leftOrRight, const char * log)
{
    u8 i, j;
    if (leftOrRight == LOG_LEFT) {
        for (i = 0; i < LINE_LEN - 1; i++) {
            for (j = 0; j < CHAR_LEN; j++) {
                logArrayL[i][j] = logArrayL[i + 1][j];
            }
            PrintLog(leftOrRight, i, & logArrayL[i][0]);
        }
        for (j = 0; j < CHAR_LEN; j++) {
            logArrayL[LINE_LEN - 1][j] = * log;
            log++;
        }
        PrintLog(leftOrRight, LINE_LEN - 1, & logArrayL[LINE_LEN - 1][0]);
    } else {
        for (i = 0; i < LINE_LEN - 1; i++) {
            for (j = 0; j < CHAR_LEN; j++) {
                logArrayR[i][j] = logArrayR[i + 1][j];
            }
            PrintLog(leftOrRight, i, & logArrayR[i][0]);
        }
        for (j = 0; j < CHAR_LEN; j++) {
            logArrayR[LINE_LEN - 1][j] = * log;
            log++;
        }
        PrintLog(leftOrRight, LINE_LEN - 1, & logArrayR[LINE_LEN - 1][0]);
    }
}

static void PrintLog(LOG_SIDE leftOrRight, u8 lineNo, char * s)
{
    if (leftOrRight == LOG_LEFT) {
        Gui_Fill_Rec(LOG_LEFT_START, Y_LOG + lineNo * 16, LOG_LEFT_END, Y_LOG + (lineNo + 1) * 16 - 1, BackgroundColor_Log);
        Gui_DrawFont_ASC16X08(LOG_LEFT_START, Y_LOG + lineNo * 16, FrontColor_Log_R, BackgroundColor_Log, s);
    } else {
        Gui_Fill_Rec(LOG_RIGHT_START, Y_LOG + lineNo * 16, LOG_RIGHT_END, Y_LOG + (lineNo + 1) * 16 - 1, BackgroundColor_Log);
        Gui_DrawFont_ASC16X08(LOG_RIGHT_START, Y_LOG + lineNo * 16, FrontColor_Log_R, BackgroundColor_Log, s);
    }
}
