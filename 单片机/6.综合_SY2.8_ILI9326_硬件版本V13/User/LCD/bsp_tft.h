//2.8寸TFT模块
#ifndef __BSP_TFT_TM022HDH26_H
#define __BSP_TFT_TM022HDH26_H
#include "stm32f10x.h"

#define H_VIEW  0  //1横屏显示, 0正常显示
//红色系
#define ROSE        0x7800
#define CARMINE     0x700B
#define RED         0xf800
//蓝色系
#define BLUE        0x001f
#define DARK_BLUE   0x0010

#define WHITE       0xffff
#define BLACK       0x0000
#define GRAY0       0xEF7D
#define YELLOW      0xFFE0

#define GREEN       0x07e0
#define GRAY1       0x8410  //灰色1      00000 000000 00000
#define GRAY2       0x4208  //灰色2  1111111111011111
//#define CYAN    0x7FFF
#define MAGENTA     0xF81F

#define X_MAX_PIXEL         240
#define Y_MAX_PIXEL         400

//默认程序接线说明：
//数据端口，只用到了PD0-7
#define LCD_DATA_PORT       GPIOD
#define LCD_DATA_CLK        RCC_APB2Periph_GPIOD
#define LCD_DATA_PIN        GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7

//片选端口
#define LCD_CS_PORT         GPIOE
#define LCD_CS_CLK          RCC_APB2Periph_GPIOE
#define LCD_CS_PIN          GPIO_Pin_2
//数据/命令
#define LCD_RS_PORT         GPIOE
#define LCD_RS_CLK          RCC_APB2Periph_GPIOE
#define LCD_RS_PIN          GPIO_Pin_3
//写数据
#define LCD_WR_PORT         GPIOE
#define LCD_WR_CLK          RCC_APB2Periph_GPIOE
#define LCD_WR_PIN          GPIO_Pin_4
//读数据
#define LCD_RD_PORT         GPIOE
#define LCD_RD_CLK          RCC_APB2Periph_GPIOE
#define LCD_RD_PIN          GPIO_Pin_5
////复位
#define LCD_RST_PORT        GPIOE
#define LCD_RST_CLK         RCC_APB2Periph_GPIOE
#define LCD_RST_PIN         GPIO_Pin_6
//背光
#define LCD_LED_PORT        GPIOE
#define LCD_LED_CLK         RCC_APB2Periph_GPIOE
#define LCD_LED_PIN         GPIO_Pin_15

//液晶控制口置1操作语句宏定义
#define LCD_CS_SET  LCD_CS_PORT->BSRR = LCD_CS_PIN
#define LCD_RS_SET  LCD_RS_PORT->BSRR = LCD_RS_PIN
#define LCD_WR_SET  LCD_WR_PORT->BSRR = LCD_WR_PIN
#define LCD_RD_SET  LCD_RD_PORT->BSRR = LCD_RD_PIN
#define LCD_LED_SET LCD_LED_PORT->BSRR= LCD_LED_PIN
#define LCD_RST_SET LCD_RST_PORT->BSRR =LCD_RST_PIN

//液晶控制口置0操作语句宏定义
#define LCD_CS_CLR  LCD_CS_PORT->BRR = LCD_CS_PIN
#define LCD_RS_CLR  LCD_RS_PORT->BRR = LCD_RS_PIN
#define LCD_WR_CLR  LCD_WR_PORT->BRR = LCD_WR_PIN
#define LCD_RD_CLR  LCD_RD_PORT->BRR = LCD_RD_PIN
#define LCD_LED_CLR LCD_LED_PORT->BRR= LCD_LED_PIN
#define LCD_RST_CLR LCD_RST_PORT->BRR =LCD_RST_PIN

//数据传输，因为操作频繁，为了提高速度，直接用寄存器操作。
#define DATAOUT(data) LCD_DATA_PORT->ODR = ((LCD_DATA_PORT->ODR)&0xFF00) + (data)  //数据输出：高8位不变，低8位赋值
#define DATAIN        LCD_DATA_PORT->IDR         //数据输入

//====================内部调用函数====================//
//用宏定义提高速度
//往总线写数据
#define LCD_WR_DATA(data){\
    LCD_RS_SET;\
    LCD_CS_CLR;\
    DATAOUT(((data)&0xff00)>>8);\
    LCD_WR_CLR;\
    LCD_WR_SET;\
    DATAOUT(0x00ff&(data));\
    LCD_WR_CLR;\
    LCD_WR_SET;\
    LCD_CS_SET;\
}
//写寄存器
#define LCD_WR_REG(index){\
    LCD_RS_CLR;\
    LCD_CS_CLR;\
    DATAOUT(((index) & 0xff00) >> 8);\
    LCD_WR_CLR;\
    LCD_WR_SET;\
    DATAOUT((index) & 0x00ff);\
    LCD_WR_CLR;\
    LCD_WR_SET;\
    LCD_CS_SET;\
}
//往寄存器写入数据
#define LCD_WriteReg(index, data){\
    LCD_WR_REG((index));\
    LCD_WR_DATA((data));\
}

#define LCD_SetCursor(x, y){\
    LCD_WriteReg(R200, x);\
    LCD_WriteReg(R201, y);\
}

static void Lcd_GpioConfig(void);
static void TFT_Delay_us(u16 time);
static void TFT_Delay_ms(u16 time);
static u16 LCD_ReadReg(u16 index);
static u16 LCD_BGR2RGB(u16 color);

//====================外部调用函数====================//
//开始写GRAM
#define LCD_WriteRAM_Prepare(){\
    LCD_WR_REG(R202);\
}
//LCD写GRAM
#define LCD_WriteRAM(data){\
    LCD_WR_DATA(RGB_Code);\
}
//画点函数
#define LCD_DrawPoint(x, y, color){\
    LCD_SetCursor(x, y);\
    LCD_WR_REG(R202);\
    LCD_WR_DATA(color);\
}

u16 LCD_ReadPoint(u16 x, u16 y);

void bsp_InitLcd(void);     //初始化函数
void bsp_ResetLcd(void);    //
void Lcd_Clear(u16 color);  //清屏函数
void Gui_Fill_Rec(u16 x0, u16 y0, u16 x1, u16 y1, u16 color);//填充某一块区域
void Lcd_SetRegion(u16 x_start, u16 y_start, u16 x_end, u16 y_end);//设置操作区域

extern u16 DeviceCode;



//9326 LCD寄存器
#define R0             0x00
#define R1             0x01
#define R2             0x02
#define R3             0x03
#define R4             0x04
#define R5             0x05
#define R6             0x06
#define R7             0x07
#define R8             0x08
#define R9             0x09
#define R10            0x0A
#define R12            0x0C
#define R13            0x0D
#define R14            0x0E
#define R15            0x0F
#define R16            0x10
#define R17            0x11
#define R18            0x12
#define R19            0x13
#define R20            0x14
#define R21            0x15
#define R22            0x16
#define R23            0x17
#define R24            0x18
#define R25            0x19
#define R26            0x1A
#define R27            0x1B
#define R28            0x1C
#define R29            0x1D
#define R30            0x1E
#define R31            0x1F
#define R32            0x20
#define R33            0x21
#define R34            0x22
#define R36            0x24
#define R37            0x25
#define R40            0x28
#define R41            0x29
#define R43            0x2B
#define R45            0x2D
#define R48            0x30
#define R49            0x31
#define R50            0x32
#define R51            0x33
#define R52            0x34
#define R53            0x35
#define R54            0x36
#define R55            0x37
#define R56            0x38
#define R57            0x39
#define R59            0x3B
#define R60            0x3C
#define R61            0x3D
#define R62            0x3E
#define R63            0x3F
#define R64            0x40
#define R65            0x41
#define R66            0x42
#define R67            0x43
#define R68            0x44
#define R69            0x45
#define R70            0x46
#define R71            0x47
#define R72            0x48
#define R73            0x49
#define R74            0x4A
#define R75            0x4B
#define R76            0x4C
#define R77            0x4D
#define R78            0x4E
#define R79            0x4F
#define R80            0x50
#define R81            0x51
#define R82            0x52
#define R83            0x53
#define R96            0x60
#define R97            0x61
#define R106           0x6A
#define R118           0x76
#define R128           0x80
#define R129           0x81
#define R130           0x82
#define R131           0x83
#define R132           0x84
#define R133           0x85
#define R134           0x86
#define R135           0x87
#define R136           0x88
#define R137           0x89
#define R139           0x8B
#define R140           0x8C
#define R141           0x8D
#define R143           0x8F
#define R144           0x90
#define R145           0x91
#define R146           0x92
#define R147           0x93
#define R148           0x94
#define R149           0x95
#define R150           0x96
#define R151           0x97
#define R152           0x98
#define R153           0x99
#define R154           0x9A
#define R157           0x9D
#define R192           0xC0
#define R193           0xC1
#define R200               0x0200
#define R201               0x0201
#define R202               0x0202
#define R229           0xE5

#endif
