
#include "bsp_tft.h"
#include <stdio.h>

u16 DeviceCode;

/****************************************************************************
* 名    称：static void TFT_Delay_us, TFT_Delay_ms,
* 功    能：TFT延时函数
* 入口参数：时间
* 出口参数：无
* 说    明：内部调用函数
****************************************************************************/
static void TFT_Delay_us(u16 time)
{
    u16 i = 0;
    while (time--) {
        i = 10;
        while (i--);
    }
}
static void TFT_Delay_ms(u16 time)
{
    u16 i = 0;
    while (time--) {
        i = 12000;
        while (i--);
    }
}

/****************************************************************************
* 名    称：void Lcd_GpioConfig(void)
* 功    能：初始化引脚
* 入口参数：无
* 出口参数：无
* 说    明：内部调用函数
****************************************************************************/
static void Lcd_GpioConfig(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    /* 打开GPIO时钟 */
    RCC_APB2PeriphClockCmd(LCD_DATA_CLK | LCD_CS_CLK | LCD_RS_CLK
                           | LCD_WR_CLK | LCD_RD_CLK | LCD_RST_CLK
                           | LCD_LED_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;   /* 设为推挽模式 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* IO口最大速度 */
    GPIO_InitStructure.GPIO_Pin = LCD_DATA_PIN;
    GPIO_Init(LCD_DATA_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_CS_PIN;
    GPIO_Init(LCD_CS_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_RS_PIN;
    GPIO_Init(LCD_RS_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_WR_PIN;
    GPIO_Init(LCD_WR_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_RD_PIN;
    GPIO_Init(LCD_RD_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_RST_PIN;
    GPIO_Init(LCD_RST_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = LCD_LED_PIN;
    GPIO_Init(LCD_LED_PORT, & GPIO_InitStructure);
    GPIO_SetBits(LCD_DATA_PORT, LCD_DATA_PIN);
    GPIO_SetBits(LCD_CS_PORT, LCD_CS_PIN);
    GPIO_SetBits(LCD_RS_PORT, LCD_RS_PIN);
    GPIO_SetBits(LCD_WR_PORT, LCD_WR_PIN);
    GPIO_SetBits(LCD_RD_PORT, LCD_RD_PIN);
}

/****************************************************************************
* 名    称u16 LCD_ReadReg(u16 index)
* 功    能：读寄存器
* 入口参数：寄存器地址
* 出口参数：无
* 说    明：内部调用函数
****************************************************************************/
static u16 LCD_ReadReg(u16 index)
{
    u16 data = 0;
    LCD_WR_REG(index);  //写入要读的寄存器号
    LCD_DATA_PORT->CRL = 0x88888888; //PD0-7  上拉输入
    //LCD_DATA_PORT->CRH = 0X88888888; //PD8-15 上拉输入
    LCD_DATA_PORT->ODR |= 0x00FF;    //低8位输出高，高8位不变
    LCD_RS_SET;
    LCD_WR_SET;
    LCD_CS_CLR;
    //读取数据(读寄存器时,并不需要读2次)
    LCD_RD_CLR;
    TFT_Delay_us(5);//FOR 8989,延时10us
    LCD_RD_SET;
    data = (DATAIN & 0x00FF) << 8;
    LCD_RD_CLR;
    TFT_Delay_us(5);//FOR 8989,延时10us
    LCD_RD_SET;
    data |= (DATAIN & 0x00FF);
    LCD_CS_SET;
    LCD_DATA_PORT->CRL = 0X33333333; //PD0-7  上拉输出
    //LCD_DATA_PORT->CRH = 0X33333333; //PD8-15 上拉输出
    LCD_DATA_PORT->ODR |= 0x00FF;  //低8位输出高，高8位不变
    return data;
}

/****************************************************************************
* 名    称u16 LCD_BGR2RGB(u16 color)
* 功    能：GBR格式的颜色值
* 入口参数：寄存器地址
* 出口参数：RGB格式的颜色值
* 说    明：从ILI93xx读出的数据为GBR格式，而我们写入的时候为RGB格式。
****************************************************************************/
u16 LCD_BGR2RGB(u16 color)
{
    u16  r, g, b, rgb;
    b = (color >> 0) & 0x1f;
    g = (color >> 5) & 0x3f;
    r = (color >> 11) & 0x1f;
    rgb = (b << 11) + (g << 5) + (r << 0);
    return(rgb);
}

/****************************************************************************
* 名    称u16 LCD_ReadPoint(u16 x, u16 y)
* 功    能：读取个某点的颜色值
* 入口参数：坐标 x:0-239, y:0-319
* 出口参数：color值
* 说    明：
****************************************************************************/
u16 LCD_ReadPoint(u16 x, u16 y)
{
    u16 t;
#if H_VIEW  //横屏
    u16 tmp;
    tmp = x;
    x = y;
    y = tmp;
#endif
    if (x >= X_MAX_PIXEL || y >= Y_MAX_PIXEL) {
        return 0;    //超过了范围,直接返回
    }
    LCD_SetCursor(x, y);
    LCD_WR_REG(R34);       //选择GRAM地址
    LCD_DATA_PORT->CRL = 0X88888888; //PB0-7  上拉输入
    //LCD_DATA_PORT->CRH = 0X88888888; //PB8-15 上拉输入
    LCD_DATA_PORT->ODR |= 0x00FF;   //低8位输出高，高8位不变
    LCD_RS_SET;
    LCD_CS_CLR;
    //读取数据(读GRAM时,需要读2次)
    LCD_RD_CLR;
    LCD_RD_SET;
    TFT_Delay_us(5);//FOR 9320,延时10us
    //dummy READ
    LCD_RD_CLR;
    TFT_Delay_us(5);//FOR 8989,延时10us
    LCD_RD_SET;
    t = ((DATAIN & 0x00FF) << 8) | ((DATAIN & 0xFF00) >> 8); //?????????没有确认
    LCD_CS_SET;
    LCD_DATA_PORT->CRL = 0X33333333; //PB0-7  上拉输出
    //LCD_DATA_PORT->CRH = 0X33333333; //PB8-15 上拉输出
    LCD_DATA_PORT->ODR |= 0x00FF;  //低8位输出高，高8位不变
    if (DeviceCode == 0X4535 || DeviceCode == 0X4531 || DeviceCode == 0X8989 || DeviceCode == 0XB505) {
        return t;    //这几种IC直接返回颜色值
    } else {
        return LCD_BGR2RGB(t);
    }
}

/****************************************************************************
* 名    称：void bsp_InitLcd(void)
* 功    能：初始化TFT
* 入口参数：无
* 出口参数：无
* 说    明：初始化TFT：QDtech_2.2寸天马串口模块
****************************************************************************/
void bsp_InitLcd(void)
{
    Lcd_GpioConfig();
    //复位TFT
    LCD_RST_SET;
    TFT_Delay_ms(100); // Delay 50ms
    LCD_RST_CLR;
    TFT_Delay_ms(100); // Delay 50ms
    LCD_RST_SET;
    TFT_Delay_ms(50);
    LCD_WriteReg(0x0000, 0x0001);
    TFT_Delay_ms(50);
    DeviceCode = LCD_ReadReg(0x0000);
    //    printf("LCD ID:%x\r\n", DeviceCode); //打印LCD ID
    if (DeviceCode == 0x9326) {
        //ILI9326  ID:9326
        LCD_WriteReg(0x0702, 0x3008);// Set internal timing, don’t change this value
        LCD_WriteReg(0x0705, 0x0036);// Set internal timing, don’t change this value
        LCD_WriteReg(0x070B, 0x1213);// Set internal timing, don’t change this value
        LCD_WriteReg(0x0001, 0x0100);// set SS and SM bit
        LCD_WriteReg(0x0002, 0x0100);// set 1 line inversion
        LCD_WriteReg(0x0003, 0x1030);// set GRAM write direction and BGR=1.
        LCD_WriteReg(0x0008, 0x0202);// set the back porch and front porch
        LCD_WriteReg(0x0009, 0x0000);// set non-display area refresh cycle ISC[3:0]
        LCD_WriteReg(0x000C, 0x0000);// RGB interface setting
        LCD_WriteReg(0x000F, 0x0000);// RGB interface polarity
        //Power On sequence
        LCD_WriteReg(0x0100, 0x0000);// SAP, BT[3:0], AP, DSTB, SLP, STB
        LCD_WriteReg(0x0102, 0x0000);// VREG1OUT voltage
        LCD_WriteReg(0x0103, 0x0000); // VDV[4:0] for VCOM amplitude
        TFT_Delay_ms(200); // Dis-charge capacitor power voltage
        LCD_WriteReg(0x0100, 0x1190); // SAP, BT[3:0], AP, DSTB, SLP, STB
        LCD_WriteReg(0x0101, 0x0227); // DC1[2:0], DC0[2:0], VC[2:0]
        TFT_Delay_ms(50); // Delay 50ms
        LCD_WriteReg(0x0102, 0x01BD); // VREG1OUT voltage
        TFT_Delay_ms(50); // Delay 50ms
        LCD_WriteReg(0x0103, 0x2D00); // VDV[4:0] for VCOM amplitude
        LCD_WriteReg(0x0281, 0x000E); // VCM[5:0] for VCOMH
        TFT_Delay_ms(50); // Delay 50ms
        LCD_WriteReg(0x0200, 0x0000); // GRAM horizontal Address
        LCD_WriteReg(0x0201, 0x0000); // GRAM Vertical Address
        // ----------- Adjust the Gamma Curve ----------//
        LCD_WriteReg(0x0300, 0x0000);
        LCD_WriteReg(0x0301, 0x0707);
        LCD_WriteReg(0x0302, 0x0606);
        LCD_WriteReg(0x0305, 0x0000);
        LCD_WriteReg(0x0306, 0x0D00);
        LCD_WriteReg(0x0307, 0x0706);
        LCD_WriteReg(0x0308, 0x0005);
        LCD_WriteReg(0x0309, 0x0007);
        LCD_WriteReg(0x030C, 0x0000);
        LCD_WriteReg(0x030D, 0x000A);
        //------------------ Set GRAM area ---------------//
        LCD_WriteReg(0x0210, 0x0000);// Horizontal GRAM Start Address
        LCD_WriteReg(0x0211, 0x00EF);// Horizontal GRAM End Address
        LCD_WriteReg(0x0212, 0x0000);// Vertical GRAM Start Address
        LCD_WriteReg(0x0213, 0x01AF);// Vertical GRAM Start Address
        LCD_WriteReg(0x0400, 0x3100);// Gate Scan Line 400 lines
        LCD_WriteReg(0x0401, 0x0001);// NDL,VLE, REV
        LCD_WriteReg(0x0404, 0x0000);// set scrolling line
        //-------------- Partial Display Control ---------//
        LCD_WriteReg(0x0500, 0x0000);//Partial Image 1 Display Position
        LCD_WriteReg(0x0501, 0x0000);//Partial Image 1 RAM Start/End Address
        LCD_WriteReg(0x0502, 0x0000);//Partial Image 1 RAM Start/End Address
        LCD_WriteReg(0x0503, 0x0000);//Partial Image 2 Display Position
        LCD_WriteReg(0x0504, 0x0000);//Partial Image 2 RAM Start/End Address
        LCD_WriteReg(0x0505, 0x0000);//Partial Image 2 RAM Start/End Address
        //-------------- Panel Control -------------------//
        LCD_WriteReg(0x0010, 0x0010);//DIVI[1:0];RTNI[4:0]
        LCD_WriteReg(0x0011, 0x0600);//WI[2:0];SDTI[2:0]
        LCD_WriteReg(0x0020, 0x0002);//DIVE[1:0];RTNE[5:0]
        LCD_WriteReg(0x0007, 0x0173);// 262K color and display ON
    }
    LCD_LED_SET; //点亮背光
}
void bsp_ResetLcd(void)
{
    Lcd_Clear(WHITE);   //清屏
}

/****************************************************************************
* 名    称：void Lcd_Clear(u16 color)
* 功    能：全屏清屏函数
* 入口参数：填充颜色COLOR
* 出口参数：无
* 说    明：
****************************************************************************/
void Lcd_Clear(u16 color)
{
    u32 index = 0;
    if (DeviceCode == 0X9326) {
        LCD_WriteReg(0x0200, 0);
        LCD_WriteReg(0x0201, 0);
        LCD_WR_REG(0x0202);
        for (index = 0; index < 96000; index++) {
            LCD_WR_DATA(color);
        }
    } else {
        LCD_SetCursor(0x00, 0x0000); //设置光标位置
        LCD_WriteRAM_Prepare();      //开始写入GRAM
        for (index = 0; index < 76800; index++) {
            LCD_WR_DATA(color);
        }
    }
}

/****************************************************************************
* 名    称：void Gui_Fill_Rec(u16 x0, u16 y0, u16 x1, u16 y1, u16 color)
* 功    能：填充区域
* 入口参数：起始点、终点的坐标，颜色
* 出口参数：无
* 说    明：
****************************************************************************/
void Gui_Fill_Rec(u16 x0, u16 y0, u16 x1, u16 y1, u16 color)
{
    u16 i, j;
    u16 xlen = 0;
#if H_VIEW  //横屏
    u16 tmp;
    tmp = x0;
    x0 = y0;
    y0 = tmp;
    tmp = x1;
    x1 = y1;
    y1 = tmp;
#endif
    if (x1 >= X_MAX_PIXEL || y1 >= Y_MAX_PIXEL) {
        return;    //超过了范围,直接返回
    }
    xlen = x1 - x0 + 1;
    for (i = y0; i <= y1; i++) {
        LCD_SetCursor(x0, i);   //设置光标位置
        LCD_WriteRAM_Prepare(); //开始写入GRAM
        for (j = 0; j < xlen; j++) {
            LCD_WR_DATA(color); //设置光标位置
        }
    }
}
