
#ifndef __BSP_LCD_GUI_H
#define __BSP_LCD_GUI_H
#include "bsp_tft.h"

#define LINE_LEN    5           //LOG区总的显示行数
#define CHAR_LEN    14          //LOG区每行可显示最大字符数
#define SEG_END_SIGN    0xFF    //数码管数组结束标识

#define BorderLineColor_V   GREEN   //电压边框颜色
#define BorderLineColor_A   YELLOW  //电流边框颜色
#define BackgroundColor_Log GRAY1   //LOG区背景色
#define FrontColor_Log_R    WHITE   //左侧LOG区文字颜色
#define FrontColor_Log_L    WHITE   //右侧LOG区文字颜色

//定义每行的起始Y坐标
#define Y_LINE1     16
#define Y_LINE2     52
#define Y_LINE3     88

#define Y_LINE4     152
#define Y_LINE5     188
#define Y_LINE6     224

#define Y_LINE7     280

#define Y_LINE8     320
#define Y_LINE9     336
#define Y_LINE10    352
#define Y_LINE11    368
#define Y_LINE12    384

//定义电压、电流、温度相关显示信息的行号
#define Y_VOL       Y_LINE1
#define Y_VOL_SET   Y_LINE2
#define Y_VOL_GET   Y_LINE3
#define Y_CUR       Y_LINE4
#define Y_CUR_SET   Y_LINE5
#define Y_CUR_GET   Y_LINE6
#define Y_TEMP      Y_LINE7
#define Y_LOG       Y_LINE8
//定义电压、电流、温度相关显示信息的X坐标
#define X_VOL       16  //“电压”、“设定”、“实际”字符的X坐标
#define X_VOL_STEP  104 //电压步进值X坐标
#define X_VOL_SET   80  //设定电压X坐标
#define X_VOL_GET   80  //当前电压X坐标

#define X_CUR       16  //“电流”、“设定”、“实际”字符的X坐标
#define X_CUR_STEP  104 //电流步进值X坐标
#define X_CUR_SET   80  //设定电流X坐标
#define X_CUR_GET   80  //当前电流X坐标

#define X_TEMP      16  //“温度”X坐标
#define X_TEMP_GET  104 //当前温度值X坐标
//定义左右LOG区域起始X坐标
#define LOG_LEFT_START  0
#define LOG_LEFT_END    111
#define LOG_RIGHT_START 128
#define LOG_RIGHT_END   239

typedef enum {
    LOG_LEFT = 0,
    LOG_RIGHT,
} LOG_SIDE;

static void PrintLog(LOG_SIDE leftOrRight, u8 lineNo, char * s);

extern char prevLog[];
extern char currentLog[];

extern unsigned char voltageSetArray[];
extern unsigned char currentSetArray[];
extern unsigned char voltageStep[];
extern unsigned char currentStep[];
extern unsigned char temprature[];
extern unsigned char voltageGetArray[];
extern unsigned char currentGetArray[];

void Gui_DrawFont_HZ32X32(u16 x, u16 y, u16 fc, u16 bc, char * s);
void Gui_DrawFont_Num32X24(u16 x, u16 y, u16 fc, u16 bc, u8 * num);
void Gui_DrawFont_ASC16X08(u16 x, u16 y, u16 fc, u16 bc, char * s);
void Gui_DrawFont_ASC24X16(u16 x, u16 y, u16 fc, u16 bc, char * s);

//外部调用函数
void DrawVoltageBox(u16 color);//画边框
void DrawCurrentBox(u16 color);//画边框
void ShowInterface(void);
void ShowCalibrate(u8 voltageOrCurrent);
void ShowVoltageStep(u8 iStep);//显示步进电压值：单位0.1V
void ShowCurrentStep(u8 iStep);//显示步进电流值：单位0.01A
void ShowSetVoltage(u16 iVoltage);
void ShowGetVoltage(float fVoltage);
void ShowSetCurrent(u16 iCurrent);
void ShowGetCurrent(float fCurrent);
void ShowGetTemperature(float fTemp);
void AddNewLog(LOG_SIDE leftOrRight, const char * log);
#endif
