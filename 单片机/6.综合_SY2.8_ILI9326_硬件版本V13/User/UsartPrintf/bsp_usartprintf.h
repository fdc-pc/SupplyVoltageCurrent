#ifndef __BSP_USART_PRINTF_H
#define __BSP_USART_PRINTF_H

#include "stm32f10x.h"
#include <stdio.h>

#define COM1    0x01
#define COM3    0x02
#define COM_PORT    COM3

#if COM_PORT & COM1
#define USARTn      USART1
#elif COM_PORT&COM3
#define USARTn      USART3
#endif

void bsp_InitUsartPrintf(void);

#endif

