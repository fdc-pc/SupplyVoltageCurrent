
/* Includes ------------------------------------------------------------------*/
#include "bsp_usartprintf.h"

/****************************************************************************
* 名    称：void bsp_InitUsart(void)
* 功    能：初始化USART引脚
* 入口参数：无
* 出口参数：无
* 说    明：外部调用函数
****************************************************************************/
void bsp_InitUsartPrintf(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
#if COM_PORT & COM1
    /* 第1步：打开GPIO和USART部件的时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    /* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, & GPIO_InitStructure);
    /* 第3步：将USART Rx的GPIO配置为浮空输入模式
        由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
        但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
    */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, & GPIO_InitStructure);
    /*  第3步已经做了，因此这步可以不做
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    */
    GPIO_Init(GPIOA, & GPIO_InitStructure);
#elif COM_PORT & COM3
    //    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
    //    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    //    GPIO_PinRemapConfig(GPIO_PartialRemap_USART3 , ENABLE);
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //
    //    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    //    GPIO_Init(GPIOC, & GPIO_InitStructure);
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; //
    //    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    //    GPIO_Init(GPIOC, & GPIO_InitStructure);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    GPIO_PinRemapConfig(GPIO_FullRemap_USART3 , ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; //
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOD, & GPIO_InitStructure);
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //
    //    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    //    GPIO_Init(GPIOD, & GPIO_InitStructure);
    //
    //
    //    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE );
    //    /* Configure USART2 Tx (PC.10) as alternate function push-pull */
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    //    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //    GPIO_Init(GPIOC, &GPIO_InitStructure);
    //    /* Configure USART2 Rx (PC.11) as input floating */
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    //    GPIO_Init(GPIOC, &GPIO_InitStructure);
    //    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE );
    //    USART_InitStructure.USART_BaudRate            = 115200;
    //    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    //    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    //    USART_InitStructure.USART_Parity              = USART_Parity_No ;
    //    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    //    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    //    USART_Init(UART4, &USART_InitStructure);
    //    USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
    //    USART_Cmd(UART4, ENABLE);
#endif
    /* 第4步：配置USART参数
        - BaudRate = 115200 baud
        - Word Length = 8 Bits
        - One Stop Bit
        - No parity
        - Hardware flow control disabled (RTS and CTS signals)
        - Receive and transmit enabled
    */
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx;
    USART_Init(USARTn, & USART_InitStructure);
    /* 第5步：使能 USART， 配置完毕 */
    USART_Cmd(USARTn, ENABLE);
    /* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
        如下语句解决第1个字节无法正确发送出去的问题 */
    USART_ClearFlag(USARTn, USART_FLAG_TC);     /* 清发送外城标志，Transmission Complete flag */
}

/****************************************************************************
* 名    称：int fputc(int ch, FILE * f)
* 功    能：重定义putc函数，这样可以使用printf函数从串口1打印输出
* 入口参数：无
* 出口参数：无
* 说    明：外部调用函数
****************************************************************************/
int fputc(int ch, FILE * f)
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(USARTn, (uint8_t) ch);
    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(USARTn, USART_FLAG_TC) == RESET)
    {}
    return ch;
}

/****************************************************************************
* 名    称：int fgetc(FILE * f)
* 功    能：重定义getc函数，这样可以使用scanff函数从串口1输入数据
* 入口参数：无
* 出口参数：无
* 说    明：外部调用函数
****************************************************************************/
int fgetc(FILE * f)
{
    /* 等待串口1输入数据 */
    while (USART_GetFlagStatus(USARTn, USART_FLAG_RXNE) == RESET)
    {}
    return (int)USART_ReceiveData(USARTn);
}
