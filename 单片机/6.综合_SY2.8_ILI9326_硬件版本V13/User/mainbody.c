
#include "mainbody.h"

/*********************************************************************************
* 函数名称: Run()
* 功    能: 计算AD采样的平均值并拆分送显示
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void Run(void)
{
    if (UPDATE_FLAG) { //如果定时采集时间到且不禁止更新
        UPDATE_FLAG = 0;
        //获取当前的电压、电流、温度值
        bsp_GetVoltageCurrentTemp( & fVoltageGet, & fCurrentGet, & fTempGet);
        if (fTempGet > ONFAN) {
            FAN_ON();    //超过45度开风机
        }
        if ((fTempGet < OFFFAN)&&(fanCtrlFlag==2)) {
            FAN_OFF();    //低于40度关风机
        }
        if (fTempGet > HOTDOWN) {//超过80度进入过热保护
            FAN_ON();
            OUT_OFF();      //关输出
        }
        ShowGetTemperature(fTempGet);
        ShowGetVoltage(fVoltageGet);
        ShowGetCurrent(fCurrentGet);
    }
}

/*********************************************************************************
* 函数名称: OutPut(void)
* 功    能: 假如没有按OUTPUT按键就在此等待,显示上次关机前的电压电流值，并闪动表示等待打开输出
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void OutPut(void)
{
    u16 i;
    u8  flag = 1;
    outputBtnPressed = 0;
    OUT_OFF();      //关输出
    DAC_SetChannel1Data(DAC_Align_12b_R, 0); //电压DAC清零
    DAC_SetChannel2Data(DAC_Align_12b_R, 0); //电流DAC清零
    ShowGetVoltage(0);
    ShowGetCurrent(0);
    while (flag) {
        Run();
        ShowSetVoltage(iSetVoltage);
        ShowSetCurrent(iSetCurrent);
        for (i = 0; i < 30; i ++) {
            DelayMs(10);
            OnBtnDown();
            if (outputBtnPressed) {//输出使能
                AddNewLog(LOG_LEFT, "Output Btn");
                outputBtnPressed = 0;
                flag = 0; //跳出while
                i = 50;
                break;//跳出for
            }
        }
        Gui_Fill_Rec(X_VOL_SET, Y_VOL_SET, 223, Y_VOL_SET + 31, WHITE); //刷屏
        Gui_Fill_Rec(X_CUR_SET, Y_CUR_SET, 223, Y_CUR_SET + 31, WHITE); //刷屏
        DelayMs(100);
    }
    OUT_ON();//开输出
    DelayMs(10);
    bsp_SetDacU((float)iSetVoltage / 100);//电压送DAC
    bsp_SetRelay((float)iSetVoltage / 100);
    bsp_SetDacI((float)iSetCurrent / 1000);//电流送DAC
    ShowSetVoltage(iSetVoltage);
    ShowSetCurrent(iSetCurrent);
    DelayMs(100);
}

/*********************************************************************************
* 函数名称: Start
* 功    能: 开机
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void Start(u8 bootFlag)
{
    u8     Buff[10];//I2C 读取缓存
    if (bootFlag == 1) { //将调试好的AD/DA值写入EEPROM
        Buff[0]  = 0x55;
        Buff[1]  = 0xAA;
        Buff[2] = (u8)(DAUcal01 >> 8); //电压0.1V高位
        Buff[3] = (u8)(DAUcal01);      //电压0.1V低位
        Buff[4] = (u8)(DAUcal30 >> 8); //最大电压高位
        Buff[5] = (u8)(DAUcal30);      //最大电压低位
        Buff[6] = (u8)(DAIcal01 >> 8); //电流0.1A高位
        Buff[7] = (u8)(DAIcal01);      //电流0.1A高位
        Buff[8] = (u8)(DAIcal4 >> 8);  //最大电流高位
        Buff[9] = (u8)(DAIcal4);       //最大电流高位
        if (I2C_BufferWrite(Buff, 10, 0x00, 0xa0)) { //把校准后的DA数据保存到EEPROM里面
            AddNewLog(LOG_LEFT, "Cal DA Saved");
        } else {
            AddNewLog(LOG_LEFT, "Cal Save Failed");
        }
        Buff[0]  = 0x55;
        Buff[1]  = 0xAA;
        Buff[2] = (u8)(ADUcal01 >> 8); //电压0.1V高位
        Buff[3] = (u8)(ADUcal01);      //电压0.1V低位
        Buff[4] = (u8)(ADUcal30 >> 8); //最大电压高位
        Buff[5] = (u8)(ADUcal30);      //最大电压低位
        Buff[6] = (u8)(ADIcal01 >> 8); //电流0.1A高位
        Buff[7] = (u8)(ADIcal01);      //电流0.1A高位
        Buff[8] = (u8)(ADIcal4 >> 8);  //最大电流高位
        Buff[9] = (u8)(ADIcal4);       //最大电流高位
        if (I2C_BufferWrite(Buff, 10, 0x20, 0xa0)) { //把校准后的AD数据保存到EEPROM里面
            AddNewLog(LOG_LEFT, "Cal AD Saved");
        } else {
            AddNewLog(LOG_LEFT, "Cal Save Failed");
        }
    } else { //使用之前存储的AD/DA值
        if (I2C_ReadByte(Buff, 10, 0x00, 0xa0)) { //读取DA电压电流校准后的数据
            AddNewLog(LOG_LEFT, "Load CAL DA 1");
        } else {
            AddNewLog(LOG_LEFT, "Load DA Failed");
        }
        if ((Buff[0] == 0x55) && (Buff[1] == 0xAA)) { //假如校准标志正确,就使用校准后的数据
            DAUcal01   = ((u16)Buff[2]) << 8;
            DAUcal01   = ((u16)Buff[3]) | DAUcal01;
            DAUcal30  = ((u16)Buff[4]) << 8;
            DAUcal30  = ((u16)Buff[5]) | DAUcal30;
            DAIcal01  = ((u16)Buff[6]) << 8;
            DAIcal01  = ((u16)Buff[7]) | DAIcal01;
            DAIcal4   = ((u16)Buff[8]) << 8;
            DAIcal4   = ((u16)Buff[9]) | DAIcal4;
        }
        if (I2C_ReadByte(Buff, 10, 0x20, 0xa0)) { //读取AD电压电流校准后的数据
            AddNewLog(LOG_LEFT, "Load CAL AD 1");
        } else {
            AddNewLog(LOG_LEFT, "Load DA Failed");
        }
        if ((Buff[0] == 0x55) && (Buff[1] == 0xAA)) { //假如校准标志正确,就使用校准后的数据
            ADUcal01   = ((u16)Buff[2]) << 8;
            ADUcal01   = ((u16)Buff[3]) | ADUcal01;
            ADUcal30  = ((u16)Buff[4]) << 8;
            ADUcal30  = ((u16)Buff[5]) | ADUcal30;
            ADIcal01  = ((u16)Buff[6]) << 8;
            ADIcal01  = ((u16)Buff[7]) | ADIcal01;
            ADIcal4   = ((u16)Buff[8]) << 8;
            ADIcal4   = ((u16)Buff[9]) | ADIcal4;
        }
    }
    I2C_ReadByte(Buff, 4, 0x80, 0xa0);  //读取上次断电前的电压电流值
    iSetVoltage  = (u16)Buff[0] << 8;   //高8位电压
    iSetVoltage |= (u16)Buff[1];        //和低8位组合成16位电压设定值
    iSetCurrent  = (u16)Buff[2] << 8;   //高8位电压
    iSetCurrent |= (u16)Buff[3];        //和低8位组合成16位电流设定值
    //检查如果EEPROM读出的数据非法，就初始化
    if ((iSetVoltage > MAX_VOLTAGE) || (iSetCurrent > MAX_CURRENT)) {
        iSetVoltage = 1000;
        iSetCurrent = 1000;
        Buff[0] = (u8)(iSetVoltage >> 8);       //电压高位
        Buff[1] = (u8)(iSetVoltage);            //电压低位
        Buff[2] = (u8)(iSetCurrent >> 8);       //电流高位
        Buff[3] = (u8)(iSetCurrent);            //电流高位
        I2C_BufferWrite(Buff, 4, 0x80, 0xa0);   //把调节后的数据保存到EEPROM里面
    }
    ShowSetVoltage(iSetVoltage);
    ShowSetCurrent(iSetCurrent);
    lockBtnPressed = 0;
    OutPut();
    bsp_SetDacU((float)iSetVoltage / 100);//电压送DAC
    bsp_SetRelay((float)iSetVoltage / 100);
    bsp_SetDacI((float)iSetCurrent / 1000);//电流送DAC
    ShowSetVoltage(iSetVoltage);
    ShowSetCurrent(iSetCurrent);
    OUT_ON();       //开输出
}
