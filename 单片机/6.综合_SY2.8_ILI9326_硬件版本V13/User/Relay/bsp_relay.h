#ifndef __BSP_RELAY_H
#define __BSP_RELAY_H
#include "stm32f10x.h"
#include "GpioConfig.h"

#define    RL2V_ON()       RELAY_02V_PORT->BSRR = RELAY_02V_PIN   //2V开
#define    RL2V_OFF()      RELAY_02V_PORT->BRR  = RELAY_02V_PIN   //2V关
#define    RL4V_ON()       RELAY_04V_PORT->BSRR = RELAY_04V_PIN   //4V开
#define    RL4V_OFF()      RELAY_04V_PORT->BRR  = RELAY_04V_PIN   //4V关
#define    RL8V_ON()       RELAY_08V_PORT->BSRR = RELAY_08V_PIN   //8V开
#define    RL8V_OFF()      RELAY_08V_PORT->BRR  = RELAY_08V_PIN   //8V关
#define    RL16V_ON()      RELAY_16V_PORT->BSRR = RELAY_16V_PIN   //16V开
#define    RL16V_OFF()     RELAY_16V_PORT->BRR  = RELAY_16V_PIN   //16V关

void bsp_InitRelay(void);
void bsp_ResetRelay(void);
void bsp_SetRelay(float voltage);
#endif
