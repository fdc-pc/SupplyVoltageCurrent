
#include "bsp_relay.h"

/*********************************************************************************
* 函数名称: bsp_InitRelay()
* 功    能: 初始化继电器端口
* 参    数: 无
* 返 回 值: 无
**********************************************************************************/
void bsp_InitRelay(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    /* 打开GPIO时钟 */
    RCC_APB2PeriphClockCmd(RELAY_02V_CLK | RELAY_04V_CLK | RELAY_08V_CLK
                           | RELAY_16V_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;   /* 设为推挽模式 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* IO口最大速度 */
    GPIO_InitStructure.GPIO_Pin = RELAY_02V_PIN;
    GPIO_Init(RELAY_02V_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = RELAY_04V_PIN;
    GPIO_Init(RELAY_04V_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = RELAY_08V_PIN;
    GPIO_Init(RELAY_08V_PORT, & GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = RELAY_16V_PIN;
    GPIO_Init(RELAY_16V_PORT, & GPIO_InitStructure);
}
void bsp_ResetRelay(void)
{
    RL2V_OFF();  //关2V
    RL4V_OFF();  //关4V
    RL8V_OFF();  //关8V
    RL16V_OFF(); //关16V
}

/*********************************************************************************
* 函数名称: bsp_SetRelay()
* 功    能: 根据浮点电压值，设置相应的继电器状态
* 参    数: 浮点电压
* 返 回 值: 无
**********************************************************************************/
void bsp_SetRelay(float fVoltage)
{
    RL2V_OFF();
    RL4V_OFF();
    RL8V_OFF();
    RL16V_OFF();
    if (fVoltage <= 0) {
        return;
    }
    fVoltage = (fVoltage / 1.2f) + 2; // + 2;
    //一个2V是整流桥消耗压降，
    //另一个2V是增加的电压差，留出足够的余量来保证正常稳压
    if (fVoltage >= 16) {
        fVoltage = fVoltage - 16;
        RL16V_ON();
    }
    if (fVoltage >= 8) {
        fVoltage = fVoltage - 8;
        RL8V_ON();
    }
    if (fVoltage >= 4) {
        fVoltage = fVoltage - 4;
        RL4V_ON();
    }
    if (fVoltage >= 2) {
        fVoltage = fVoltage - 2;
        RL2V_ON();
    }
}
