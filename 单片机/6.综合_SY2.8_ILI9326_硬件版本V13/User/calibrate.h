
#ifndef __BSP_CALIBRATE_H
#define __BSP_CALIBRATE_H

#include "bsp_board.h"




//外部调用函数
void CalibrateVoltage(float voltage, vu16 * adValue, vu16 * daValue);
void CalibrateCurrent(float current, vu16 * adValue, vu16 * daValue);
void Calibrate(void);//obsolete
void DisplayCalibrateGUI(vu8 calStage);

#endif
