#ifndef __GPIO_CONFIG_H
#define __GPIO_CONFIG_H

//硬件连接：
//-------------输出控制端口-------------//
#define OUTPUT_PORT         GPIOE
#define OUTPUT_CLK          RCC_APB2Periph_GPIOE
#define OUTPUT_PIN          GPIO_Pin_9

//-------------风扇控制端口-------------//
#define FAN_PORT            GPIOC
#define FAN_CLK             RCC_APB2Periph_GPIOC
#define FAN_PIN             GPIO_Pin_4

//-------------继电器控制端口-------------//
#define RELAY_02V_PORT      GPIOE
#define RELAY_02V_CLK       RCC_APB2Periph_GPIOE
#define RELAY_02V_PIN       GPIO_Pin_8

#define RELAY_04V_PORT      GPIOE
#define RELAY_04V_CLK       RCC_APB2Periph_GPIOE
#define RELAY_04V_PIN       GPIO_Pin_7

#define RELAY_08V_PORT      GPIOB
#define RELAY_08V_CLK       RCC_APB2Periph_GPIOB
#define RELAY_08V_PIN       GPIO_Pin_1

#define RELAY_16V_PORT      GPIOB
#define RELAY_16V_CLK       RCC_APB2Periph_GPIOB
#define RELAY_16V_PIN       GPIO_Pin_0

//-------------I2C端口-------------//
#define I2C_SCL_PORT      GPIOE
#define I2C_SCL_CLK       RCC_APB2Periph_GPIOE
#define I2C_SCL_PIN       GPIO_Pin_11

#define I2C_SDA_PORT      GPIOE
#define I2C_SDA_CLK       RCC_APB2Periph_GPIOE
#define I2C_SDA_PIN       GPIO_Pin_12

#define I2C_WP_PORT       GPIOE
#define I2C_WP_CLK        RCC_APB2Periph_GPIOE
#define I2C_WP_PIN        GPIO_Pin_10

//-------------74HC595端口------------//
#define HC595_SER_PORT      GPIOC
#define HC595_SER_CLK       RCC_APB2Periph_GPIOC
#define HC595_SER_PIN       GPIO_Pin_12

#define HC595_RCK_PORT      GPIOC
#define HC595_RCK_CLK       RCC_APB2Periph_GPIOC
#define HC595_RCK_PIN       GPIO_Pin_11

#define HC595_SCK_PORT      GPIOC
#define HC595_SCK_CLK       RCC_APB2Periph_GPIOC
#define HC595_SCK_PIN       GPIO_Pin_10

//-------------ADC端口------------//
#define ADC_VOLTAGE_PORT      GPIOC
#define ADC_VOLTAGE_CLK       RCC_APB2Periph_GPIOC
#define ADC_VOLTAGE_PIN       GPIO_Pin_2

#define ADC_CURRENT_PORT      GPIOC
#define ADC_CURRENT_CLK       RCC_APB2Periph_GPIOC
#define ADC_CURRENT_PIN       GPIO_Pin_3

#define ADC_TEMPERATURE_PORT      GPIOC
#define ADC_TEMPERATURE_CLK       RCC_APB2Periph_GPIOC
#define ADC_TEMPERATURE_PIN       GPIO_Pin_0

//-------------DAC端口------------//
#define DAC_VOLTAGE_PORT      GPIOA
#define DAC_VOLTAGE_CLK       RCC_APB2Periph_GPIOA
#define DAC_VOLTAGE_PIN       GPIO_Pin_4

#define DAC_CURRENT_PORT      GPIOA
#define DAC_CURRENT_CLK       RCC_APB2Periph_GPIOA
#define DAC_CURRENT_PIN       GPIO_Pin_5

//------------USART端口----------//
#define USART_TX_PORT      GPIOA
#define USART_TX_CLK       RCC_APB2Periph_GPIOA
#define USART_TX_PIN       GPIO_Pin_9

#define USART_RX_PORT      GPIOA
#define USART_RX_CLK       RCC_APB2Periph_GPIOA
#define USART_RX_PIN       GPIO_Pin_10

#endif



