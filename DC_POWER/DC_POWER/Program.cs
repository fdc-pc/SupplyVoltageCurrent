﻿using CCWin;
using System;
using System.Threading;
using System.Windows.Forms;

namespace DC_POWER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application./应用程序的主要入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*使程序只能启动一个*/
            //声明互斥体
            Mutex mutex = new Mutex(false, "DC_POWER");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //判断互斥体是否在使用中
            bool Runing = !mutex.WaitOne(0, false);
            if (!Runing)
            {
                Application.Run(new Mainform());
            }
            else
            {
                MessageBoxEx.Show("已经有一个实例运行中","温馨提示");
            }
        }
    }
}
